This tutorial provides simple examples of basic functionalities of this package.
It uses the example files contained in the package.

## GlobalModel

A `GlobalModel` object compresses a set of metabolic reconstructions by only
storing the stoichiometry of each unique reaction once. Ideally, in your workflow
you would only create a `GlobalModel` object once for each set of models.

Once a `GlobalModel` object has been created, it can be saved as a .mat file
and quickly imported.

Create and save a GlobalModel

```julia
using microbiome

modelsList = ["toyModel1.mat"; "toyModel2.mat"; "toyModel3.mat"];
pathToModels = joinpath(pathof(microbiome), "test", "data");
resultsFolder = "example";
mkdir(resultsFolder);
gm = buildGlobalModel(modelsList, pathToModels, "model");
writeGlobalModel(joinpath(resultsFolder, "globalModel.mat"), gm, "model");
```

Alternatively, you can also give the list of models as a text file (one filename per line).

```julia
modelsListFile = joinpath(pathToModels, "toyModelsList.txt");
gm = buildGlobalModel(modelsListFile, pathToModels, "model");
```

Any file format supported by `COBREXA` should work with `buildGlobalModel`.

You can now import the `GlobalModel` directly from the file.
```julia
gm = loadGlobalModel(joinpath(resultsFolder, "globalModel.mat"), "model");
```

Retrieve any individual model as a `COBREXA.CoreModel`
```julia
model1 = getModel(gm, 1);
model2 = getModel(gm, "toyModel2");
```

Merge models together to create a pan-model: the resulting model will have all the reactions of the merged models,
and a biomass reaction that is the average of the biomass requirements of the merged models.
```julia
mergdGm = mergeModels(gm, [1;2]);
mergdGm = mergeModels(gm, ["toyModel1"; "toyModel3"], "mergd1plus3");
mergd13 = getModel(mergdGm, "mergd1plus3");
```

## Community modelling

Using the `GlobalModel` structure as a scaffold, the package creates a simple compartmentalised community model. This model assumes that relative abundances of the community members are known a priori and remain stable. Balanced growth can then be modelled with a community biomass reaction which allows the use of standard COBRA tools such as FBA.

Load a `GlobalModel` and transform into a community model
```julia
gm = loadGlobalModel(joinpath(resultsFolder, "globalModel.mat"));
cm = createCommunityReactions(gm);
```

`cm` now contains an additional "model" called 'community' which has exchange
reactions connecting the actual models through their exchanges,
and a community biomass reaction.

Expand `cm` into a `COBREXA.CoreModel` by specifying relative abundances for all
models (no abundance should be given for 'community')
```julia
abundances = [0.8;0.1;0.1];
lm = expandCommunityModel(cm, abundances);
```

`expandCommunityModel` has two strategies for trying to prevent biologically implausible exchanges between community members. The default option `:abScale` multiplies the upper and lower reaction bounds of each member by their relative abundance
```julia
lm = expandCommunityModel(cm, abundances, scalingStrat=:abScale);
```
Alternatively, you can "couple" the exchange reactions of each member to their biomass reaction
```julia
lm = expandCommunityModel(cm, abundances, scalingStrat=:couple, coupBig=400, coupSmall=0);
```

`lm` is a `COBREXA.CoreModel` object, and you can run FBA etc. as you would with a normal model. You can also run FBA and FVA directly on the community model
```julia
using GLPK
(objValue, fluxes) = communityFBA(cm, abundances, GLPK.Optimizer);
fluxes = communityFVA(cm, abundances, GLPK.Optimizer);
```
but note that these will return fluxes only for the community exchange reactions.

## Microbiome modelling

The package also contains tools for the analysis of microbiome data sets. For a tutorial specifically on the use with VMH, see `tutorials/VMH-workflow.md`.

The starting point should be an abundance table whose organism identifiers match the repository of models (ie the `GlobalModel`) you are using. For correct formatting, see the example `abTableFile` below.

We import the data by creating a MetaSet object
```julia
abTableFile = joinpath(pathToModels, "abundanceTable.csv");
gModelFile = joinpath(resultsFolder, "globalModel.mat");
ms = MetaSet(abTableFile, gModelFile);
```

You can check how well each sample was covered
```julia
ms.coverage
```

If any samples were rejected because the coverage was very low (<0.001), you can see them in `ms.removedSamples`.

Run FBA for all samples and export the results
```julia
runFBA!(ms, GLPK.Optimizer);
exportMetaSet(resultsFolder, ms);
```

If your data set contains a lot of samples, you may want to split analyses into batches
```julia
ms = importMetaSet(resultsFolder, gModelFile);
splitdSets = splitMetaSet(ms, 2);
runFVA!(splitdSets[1], GLPK.Optimizer);
exportMetaSet(joinpath(resultsFolder, "example-1"), splitdSets[1]);
runFVA!(splitdSets[2], GLPK.Optimizer);
exportMetaSet(joinpath(resultsFolder, "example-2"), splitdSets[2]);
```

You can then merge your results
```julia
ms1 = importMetaSet(joinpath(resultsFolder, "example-1"), gModelFile);
ms2 = importMetaSet(joinpath(resultsFolder, "example-2"), gModelFile);
mergdSet = mergeMetaSets([ms1; ms2]);
```
Note that if some results are present only in some of the sets to merge, they will be ignored.
