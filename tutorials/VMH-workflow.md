This tutorial presents a workflow based on the Virtual Metabolic Human (VMH) [[1](#References-1)] and the AGORA collection [[2](#References-1)]. You should first download the models from https://www.vmh.life/#downloadview and diets from https://www.vmh.life/#nutrition. This tutorial uses AGORA-1.03 with mucin.

# Set-up

After you've downloaded the models you can create and save a `GlobalModel` of the collection. Note that this step takes some time (~10 minutes).
```julia
using microbiome
mkdir("example");
pathToAGORA = # put here path to the main folder containing the AGORA models
pathToDiets =
listOfModels = readdir(joinpath(pathToAGORA, "reconstructions", "mat"));
gm = buildGlobalModel(listOfModels, joinpath(pathToAGORA, "reconstructions", "mat"));
writeGlobalModel("example/AGORA.mat", gm);
```
# Simple pipeline

In order to create personalised microbiome models, you need to provide an abundance table in terms of the model IDs in the `GlobalModel` representing AGORA. This tutorial uses an example file provided in `joinpath(pathof(microbiome), "tutorial", "data", "modelAbundances.csv")`. It is purposefully truncated to only include a few models so that the resulting analyses do not take such a long time.

Run FBA and FVA for all samples, using the average European diet, and save the results
```julia
using GLPK
ms = MetaSet(joinpath(pathof(microbiome), "tutorial", "data", "modelAbundances.csv"), "example/AGORA.mat");
runFBA!(ms, GLPK.Optimizer, dietFile=joinpath(pathToDiets, "EU_average.tsv"));
runFVA!(ms, GLPK.Optimizer, dietFile=joinpath(pathToDiets, "EU_average.tsv"));
exportMetaSet("example/AGORA-example", ms);
```

# More specific examples

## Multiple results

You might want to run the same analyses again but with a different diet. If you do so on the same MetaSet, the previous results will be overwritten. However, if you've already saved them as above, you can differentiate the new results with a prefix
```julia
ms = importMetaSet("example/AGORA-example", "example/AGORA.mat");
runFBA!(ms, GLPK.Optimizer, dietFile=joinpath(pathToDiets, "Vegan.tsv"));
runFVA!(ms, GLPK.Optimizer, dietFile=joinpath(pathToDiets, "Vegan.tsv"));
exportMetaSet("example/AGORA-example", ms, filePrefix="Vegan_");
```

You can import these specific results by specifying the same prefix
```julia
ms = importMetaSet("example/AGORA-example", "example/AGORA.mat", filePrefix="Vegan_");
```

## Splitting your data set into batches

In practice, constraint-based analyses with microbiome models comprising hundreds of reconstructions will take considerable resources to run. Thus you may need to employ a high performance cluster, especially for FVA. In addition, you can easily split your data sets into batches within the package
```julia
ms = importMetaSet("example/AGORA-example", "example/AGORA.mat");
splitdSets = splitMetaSet(ms, 4);
for (i, set) in enumerate(splitdSets)
  runFBA!(set, GLPK.Optimizer, dietFile=joinpath(pathToDiets, "DACH.tsv"));
  exportMetaSet("example/AGORA-example", set, filePrefix="DACH-$i"*"_");
end
```

You can then gather and combine the results (but note that all of your batches need to have results, otherwise they will be ignored)
```julia
setsToMerge = [importMetaSet("example/AGORA-example", "example/AGORA.mat", filePrefix="DACH-$i"*"_") for i in 1:4];
mergdSet = mergeMetaSets(setsToMerge);
```

## FVA for specific metabolites

By default, `runFVA!` finds the minimum and maximum for all community exchanges, corresponding to min and max net production of metabolites by the community under the constraint of optimal growth. For the AGORA collection this amounts to some 400 reactions to minimise and maximise which can be a formidable computational task. If you'd prefer to only perform FVA on a selected set of metabolites, you can do so in the following way
```julia
using COBREXA
ms = MetaSet(joinpath(pathof(microbiome), "tutorial", "data", "modelAbundances.csv"), "example/AGORA.mat");
FVAtargets = ["EX_ppa(com)"; "EX_btoh(com)"];
runFVA!(ms, GLPK.Optimizer, reactions=FVAtargets);
```

## Suboptimal FVA

It might be of interest to not require exactly optimal growth in FVA. This can be done by varying the `gamma` parameter. Especially interesting is the case where `gamma = 0`. Here the community biomass reaction will only restrict fluxes by its stoichiometry, which constrains individual biomass production rates to conform to the community composition. It can be seen as the "raw" potential for the consumption and production of metabolites.
```julia
ms = MetaSet(joinpath(pathof(microbiome), "tutorial", "data", "modelAbundances.csv"), "example/AGORA.mat");
runFVA!(ms, GLPK.Optimizer, gamma=0.0);
```

## Creating pan-models

The models in the AGORA collection are on the strain-level. However, your data might not reach this level of classification. One option in this case is to create higher level "pan-models". Here is how to create a `GlobalModel` with genus-level models by merging all strain models within genus
```julia
gm = loadGlobalModel("example/AGORA.mat");
genusList = unique([split(row, '_')[1] for row in gm.modelIDs]);
for genus in genusList
  inds = findall(startswith.(gm.modelIDs, genus));
  gm = mergeModels(gm, inds, String(genus));
end
writeGlobalModel("example/AGORA_genus.mat", gm);
```

## Contextualised GlobalModels

In addition to creating pan-models, you can create other custom GlobalModels as well. If you'd prefer to simply have a model for each diet
```julia
gm = loadGlobalModel("example/AGORA.mat");
gm = createCommunityReactions(gm);
applyDiet!(gm, joinpath(pathToDiets, "EU_average.tsv"));
writeGlobalModel("example/AGORA_EU_average.mat", gm);
```

You can also remove models or reactions
```julia
gm = loadGlobalModel("example/AGORA.mat");
achromobacter = findall(startswith.(gm.modelIDs, "Achromobacter"));
modGm = removeModels(gm, achromobacter);
writeGlobalModel("example/AGORA_noAchromobacter.mat", modGm);

modGm = remove_reactions(gm, "ACt2r");
writeGlobalModel("example/AGORA_noACtr2.mat", modGm);
```


# References

1. [Noronha, A., Modamio, J., Jarosz, Y., Guerard, E., Sompairac, N., Preciat, G., ... & Thiele, I. (2019). The Virtual Metabolic Human database: integrating human and gut microbiome metabolism with nutrition and disease. Nucleic acids research, 47(D1), D614-D624.](https://doi.org/10.1093/nar/gky992)

2. [Magnúsdóttir, S., Heinken, A., Kutt, L., Ravcheev, D. A., Bauer, E., Noronha, A., ... & Thiele, I. (2017). Generation of genome-scale metabolic reconstructions for 773 members of the human gut microbiota. Nature biotechnology, 35(1), 81-89.](https://doi.org/10.1038/nbt.3703)
