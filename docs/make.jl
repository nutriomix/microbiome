using Documenter, microbiome

makedocs(modules = [microbiome],
        clean = false,
        sitename = "microbiome.jl",
        format = Documenter.HTML(
            # Use clean URLs, unless built as a "local" build
            prettyurls = !("local" in ARGS),
            highlights = ["yaml"],
        ),
        authors = "The developers of microbiome.jl",
        linkcheck = !("skiplinks" in ARGS),
        pages = [
                "Home" => "index.md",
                ],
        )
