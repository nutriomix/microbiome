@testset "GlobalModel type" begin
    gm = GlobalModel(test_LP(),
                        ones(1, 3),
                        ["bm"],
                        ["model"])
    @test gm isa GlobalModel
end

@testset "MetaSet type" begin
    gm = GlobalModel(test_LP(),
                    ones(2, 3),
                    ["bm"; "community_biomass"],
                    ["model"; "community"])
    ms = MetaSet([0.1 0.9], ["s1"; "s2"], ones(2), [], gm)
    @test ms isa MetaSet
end
