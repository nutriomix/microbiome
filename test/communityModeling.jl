@testset "Create community model" begin
    globalModel = buildGlobalModel(joinpath("data", "toyModelsList.txt"), "data")
    comModel = createCommunityReactions(globalModel)
    nExcs = length(find_exchange_reactions(globalModel.coreModel, exclude_biomass=true))
    @test comModel isa GlobalModel
    @test isCommunityModel(comModel)
    @test length(find_exchange_reactions(comModel.coreModel, exclude_biomass=true)) == nExcs
    @test n_reactions(comModel.coreModel) == n_reactions(globalModel.coreModel) + nExcs
    @test n_metabolites(comModel.coreModel) == n_metabolites(globalModel.coreModel) + nExcs
    @test nModels(comModel) == nModels(globalModel) + 1

    globalModel = GlobalModel(CoreModel([-1. -1  0  0;
                                            0  1 -1  0;
                                            0  0  1 -1],
                                            zeros(3),
                                            ones(4),
                                            zeros(4),
                                            ones(4),
                                            ["EX_m1(e)"; "m1t"; "biomass"; "EX_biomass(e)"],
                                            ["m1[e]"; "m1[c]"; "biomass[c]"]),
                                            ones(1, 4),
                                            ["biomass"],
                                            ["model1"])
    comModel = createCommunityReactions(globalModel)
    @test comModel.coreModel.rxns == ["m1t"; "IEX_m1(e)"; "biomass"; "EX_m1(com)";
                                            "biomass(com)"]
    @test comModel.coreModel.mets == ["m1[e]"; "m1[c]"; "m1[com]"; "biomass[sub]"]
    @test comModel.coreModel.S == [-1  -1   0   0  0;
                                      1   0  -1   0  0;

                                      0   1   0  -1  0;
                                      0   0   1   0 -1]
    @test comModel.rxnPres == [1 1 1 0 0;
                               0 0 0 1 1]
    @test !isCommunityModel(globalModel)
    @test isCommunityModel(comModel)
end

@testset "Expand GlobalModel" begin
    globalModel = GlobalModel(CoreModel([-1. -1  0  0  0  0;
                                            0  1 -1 -1  0  0;
                                            0  0  1  1 -1  0;
                                            0  0  0  0  1 -1],
                                            zeros(4),
                                            zeros(6),
                                            -[1.; 2; 3; 0; 5; 6],
                                            collect(1.:6),
                                            ["EX_m1(e)"; "m1t"; "r1"; "r2"; "biomass"; "EX_biomass(e)"],
                                            ["m1[e]"; "m1[c]"; "m2[c]"; "biomass[c]"]),
                            [1 1 1 0 1 1;
                             1 1 0 1 1 1],
                            ["biomass"; "biomass"], ["model1"; "model2"])
    comModel = createCommunityReactions(globalModel)
    cb = 400
    cs = 0.01
    expModel = expandCommunityModel(comModel, [0.9; 0.1], scalingStrat=:couple, coupBig=cb, coupSmall=cs)
    @test expModel isa CoreModelCoupled
    @test reactions(expModel) == ["model1_m1t";
                                  "model1_r1";
                                  "model1_IEX_m1(e)"
                                  "model1_biomass";
                                  "model2_m1t";
                                  "model2_r2";
                                  "model2_IEX_m1(e)";
                                  "model2_biomass";
                                  "EX_m1(com)";
                                  "biomass(com)"]
    @test metabolites(expModel) == ["model1_m1[e]";
                                    "model1_m1[c]";
                                    "model1_m2[c]";
                                    "model1_biomass[sub]";
                                    "model2_m1[e]";
                                    "model2_m1[c]";
                                    "model2_m2[c]";
                                    "model2_biomass[sub]";
                                    "m1[com]"]
    @test stoichiometry(expModel) == [-1  0 -1  0   0  0  0  0   0    0;
                                       1 -1  0  0   0  0  0  0   0    0;
                                       0  1  0 -1   0  0  0  0   0    0;
                                       0  0  0  1   0  0  0  0   0 -0.9;

                                       0  0  0  0  -1  0 -1  0   0    0;
                                       0  0  0  0   1 -1  0  0   0    0;
                                       0  0  0  0   0  1  0 -1   0    0;
                                       0  0  0  0   0  0  0  1   0 -0.1;

                                       0  0  1  0   0  0  1  0  -1    0]
    @test balance(expModel) == zeros(9)
    @test objective(expModel) == [zeros(4); zeros(4); [0;1]]
    @test bounds(expModel)[1] == [-[2; 3; 1; 5];
                                  -[2; 0; 1; 5];
                                  comModel.coreModel.xl[end-1:end]]
    @test bounds(expModel)[2] ==  [2; 3; 1; 5;
                                   2; 4; 1; 5;
                                   comModel.coreModel.xu[end-1:end]]
    @test coupling(expModel) == [0  0  1 -cb   0  0  0   0   0  0;
                                 0  0  0   0   0  0  1 -cb   0  0;

                                 0  0  1  cb   0  0  0   0   0  0;
                                 0  0  0   0   0  0  1  cb   0  0]
     sconst = -400005
     bconst = 401000
     @test minimum(bounds(expModel)[1]) - cb*maximum(bounds(expModel)[2]) == sconst
     @test maximum(bounds(expModel)[2]) + cb*maximum(bounds(expModel)[2]) == bconst
     @test coupling_bounds(expModel)[1] == [sconst;
                                            sconst;
                                            -cs;
                                            -cs]
     @test coupling_bounds(expModel)[2] == [cs;
                                            cs;
                                            bconst;
                                            bconst]

    expModel = expandCommunityModel(comModel, [0; 1.], scalingStrat=:couple, coupBig=cb, coupSmall=cs)
    @test stoichiometry(expModel) == [-1  0 -1  0   0  0;
                                       1 -1  0  0   0  0;
                                       0  1  0 -1   0  0;
                                       0  0  0  1   0 -1;

                                       0  0  1  0  -1  0]
    @test coupling(expModel) == [0  0  1 -cb   0  0;

                                 0  0  1  cb   0  0]

    abA = 0.6
    abB = 0.4
    expModel = expandCommunityModel(comModel, [abA; abB], scalingStrat=:abScale)
    @test expModel isa CoreModel
    @test expModel.xl == [-[2; 3; 1; 5] .* abA;
                          -[2; 0; 1; 5] .* abB;
                          comModel.coreModel.xl[end-1:end]]
    @test expModel.xu ==  [[2; 3; 1; 5] .* abA;
                           [2; 4; 1; 5] .* abB;
                           comModel.coreModel.xu[end-1:end]]
end

@testset "FBA" begin
    optimizer = GLPK.Optimizer
    gm = loadGlobalModel(joinpath("data", "toyCommunityModel.mat"))
    ab = [0.1;0.1;0.8]
    expModel = expandCommunityModel(gm, ab)
    ref_optModel = flux_balance_analysis(expModel, optimizer)
    ref_x = JuMP.value.(JuMP.all_variables(ref_optModel))
    (objVal, fluxes) = communityFBA(gm, ab, optimizer)
    @test JuMP.objective_value(ref_optModel) ≈ objVal
    @test value.(ref_x)[24:27] ≈ fluxes

    ab = [0.9; 0; 0.1]
    expModel = expandCommunityModel(gm, ab)
    ref_optModel = flux_balance_analysis(expModel, optimizer)
    ref_x = JuMP.value.(JuMP.all_variables(ref_optModel))
    (objVal, fluxes) = communityFBA(gm, ab, optimizer)
    @test JuMP.objective_value(ref_optModel) ≈ objVal
    @test value.(ref_x)[18:21] ≈ fluxes

    gm.coreModel.xu .= 0
    (objVal, fluxes) = communityFBA(gm, ab, optimizer)
    @test objVal == 0
end

@testset "FVA" begin
    optimizer = GLPK.Optimizer
    gm = loadGlobalModel(joinpath("data", "toyCommunityModel.mat"))
    ab = [0.1;0.1;0.8]
    expModel = expandCommunityModel(gm, ab)
    ref_fluxes = flux_variability_analysis(expModel, collect(24:27), optimizer)
    fluxes = communityFVA(gm, ab, optimizer)
    @test ref_fluxes ≈ fluxes

    ab = [0.9; 0; 0.1]
    expModel = expandCommunityModel(gm, ab)
    ref_fluxes = flux_variability_analysis(expModel, [21], optimizer)
    fluxes = communityFVA(gm, ab, ["EX_m4(com)"], optimizer)
    @test ref_fluxes ≈ fluxes

    fluxes = communityFVA(gm, [0;1.;0], GLPK.Optimizer, gamma=0.0)
    @test fluxes ≈ [0 0; -1000 0; 0 500; 0 0]
end
