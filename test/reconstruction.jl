@testset "buildGlobalModel" begin

globalModel = buildGlobalModel(joinpath("data", "toyModelsListWithMissingModel.txt"), "data")
@test globalModel isa GlobalModel
@test nModels(globalModel) == 3

globalModel = buildGlobalModel(["toyModel1.mat"; "toyModel2.mat"], "data")
@test globalModel isa GlobalModel
@test nModels(globalModel) == 2

globalModel = buildGlobalModel(joinpath("data", "toyModelsList.txt"), "data")
@test globalModel isa GlobalModel
nRxns = 13
nMets = 9
nMods = 3

lm = globalModel.coreModel
@test size(lm.S) == (nMets, nRxns)

@test length(lm.b) == nMets
@test length(lm.c) == nRxns
@test length(lm.xl) == nRxns
@test length(lm.xu) == nRxns
@test length(lm.rxns) == nRxns
@test length(lm.mets) == nMets

@test size(globalModel.rxnPres) == (nMods, nRxns)

@test Array{Int64}(globalModel.rxnPres) == [1   1   0   1   0   1   0   1   0   1   0   1   0;
                                            1   0   1   1   0   0   1   0   1   1   0   0   1;
                                            1   1   1   1   1   1   0   1   1   1   1   1   1]

modelsList = readdir(joinpath("data", "SBML"))
globalModel = buildGlobalModel(modelsList, joinpath("data", "SBML"))
@test globalModel isa GlobalModel

lm = globalModel.coreModel
@test size(lm.S) == (nMets, nRxns)

@test length(lm.b) == nMets
@test length(lm.c) == nRxns
@test length(lm.xl) == nRxns
@test length(lm.xu) == nRxns
@test length(lm.rxns) == nRxns
@test length(lm.mets) == nMets

@test size(globalModel.rxnPres) == (nMods, nRxns)

@test Array{Int64}(globalModel.rxnPres) == [1   1   0   1   0   1   0   1   0   1   0   1   0;
                                            1   0   1   1   0   0   1   0   1   1   0   0   1;
                                            1   1   1   1   1   1   0   1   1   1   1   1   1]

end

@testset "remove reactions" begin
    lp = CoreModel([1. 1 1 0; 1 1 1 0; 1 1 1 0; 0 0 0 1],
                     zeros(4),
                     zeros(4),
                     zeros(4),
                     zeros(4),
                     ["r1"; "r2"; "r3"; "r4"],
                     ["m1"; "m2"; "m3"; "m4"])
    gm = GlobalModel(lp,
                     ones(1, 4),
                     ["r3"],
                     ["model"])
    mGm = remove_reactions(gm, 2)
    @test size(mGm.coreModel.S) == (4, 3)
    @test size(mGm.rxnPres) == (1, 3)
    mGm = remove_reactions(gm, [2; 1])
    @test size(mGm.coreModel.S) == (4, 2)
    @test size(mGm.rxnPres) == (1, 2)
    mGm = remove_reactions(gm, "r0")
    @test size(mGm.coreModel.S) == (4, 4)
    mGm = remove_reactions(gm, "r2")
    @test size(mGm.coreModel.S) == (4, 3)
    @test size(mGm.rxnPres) == (1, 3)
    mGm = remove_reactions(gm, ["r2"; "r1"])
    @test size(mGm.coreModel.S) == (4, 2)
    @test size(mGm.rxnPres) == (1, 2)
    mGm = remove_reactions(gm, 3)
    @test size(mGm.coreModel.S) == (4, 4)
    @test size(mGm.rxnPres) == (1, 4)
end

@testset "Add reactions" begin
    lp = CoreModel([1. 0; 1 0],
                     zeros(2),
                     zeros(2),
                     zeros(2),
                     zeros(2),
                     ["r1"; "r2"],
                     ["m1"; "m2"])
    gm = GlobalModel(lp,
                     ones(2, 2),
                     ["r1"; "r1"],
                     ["mod1"; "mod2"])
    newGm = add_reactions(gm, [1.; 0], zeros(2), 0., 0., 0., "r3", ["m1"; "m3"], [1; 0])
    @test newGm.coreModel.rxns[3] == "r3"
    @test newGm.rxnPres[:, 3] == [1; 0]
    newGm = add_reactions(gm, [1. 0; 1. 0], zeros(2), zeros(2), zeros(2),
                            zeros(2), ["r3";"r4"], ["m1"; "m3"], [1 0; 0 1])
    @test newGm.coreModel.rxns[[3; 4]] == ["r3";"r4"]
    @test newGm.rxnPres[:, [3; 4]] == [1 0; 0 1]
    newGm = add_reactions(gm, CoreModel([1. 0; 1. 0], zeros(2), zeros(2), zeros(2),
                            zeros(2), ["r3";"r4"], ["m1"; "m3"]), [1 0; 0 1])
    @test newGm.coreModel.rxns[[3; 4]] == ["r3";"r4"]
    @test newGm.rxnPres[:, [3; 4]] == [1 0; 0 1]
end

@testset "Remove models" begin
    gm = loadGlobalModel(joinpath("data", "toyGlobalModel.mat"))
    @test_throws ArgumentError removeModels(gm, [1;4])
    newGm = removeModels(gm, [1;3])
    @test nModels(newGm) == 1
    @test newGm.rxnPres == ones(1, 7)
    newGm = removeModels(gm, ["toyModel4"])
    @test isequal(gm, newGm)
    newGm = removeModels(gm, ["toyModel1"; "toyModel4"])
    @test nModels(newGm) == 2
    @test newGm.rxnPres == [1  0  1  1  0  0  1  0  1  1  0  0  1;
                            1  1  1  1  1  1  0  1  1  1  1  1  1]
    newGm = removeModels(gm, 1)
    @test nModels(newGm) == 2
    newGm = removeModels(gm, [1;2;3])
    @test nModels(newGm) == 0
end

@testset "Merge models" begin
    gm = loadGlobalModel(joinpath("data", "toyGlobalModel.mat"))
    @test_throws ArgumentError mergeModels(gm, [1;4])
    newGm = mergeModels(gm, [1;2])
    @test nModels(newGm) == 2
    @test newGm.rxnPres == [1  1  1  1  1  1  1  1  1  1  1  1  0;
                            1  1  1  1  0  0  1  1  1  0  1  1  1]
    @test newGm.biomassRxns[2] == "toyModel1toyModel2_mergedBiomass"
    @test newGm.modelIDs[2] == "toyModel1toyModel2"
    @test newGm.coreModel.S[:, end] == [1; 0; 0; -0.5; 0; 0; 0; -0.5; 0]
    newGm = mergeModels(gm, [1;2;3], "mergd")
    @test nModels(newGm) == 1
    @test newGm.rxnPres == ones(1, 12)
    @test newGm.biomassRxns == ["mergd_mergedBiomass"]
    @test newGm.modelIDs == ["mergd"]
    newGm = mergeModels(gm, ["toyModel3"; "toyModel2"; "toyModel4"])
    @test nModels(newGm) == 2
end

@testset "Get model" begin
    gm = loadGlobalModel(joinpath("data", "toyGlobalModel.mat"))
    model1 = getModel(gm, 1)
    model1Ref = load_model(CoreModel, joinpath("data", "toyModel1.mat"))
    sortRxns = sortperm(model1Ref.rxns)
    sortMets = sortperm(model1Ref.mets)
    @test model1.S == model1Ref.S[sortMets, sortRxns] &&
            model1.b == model1Ref.b[sortMets] &&
            model1.c == model1Ref.c[sortRxns] &&
            model1.xl == model1Ref.xl[sortRxns] &&
            model1.xu == model1Ref.xu[sortRxns] &&
            model1.rxns == model1Ref.rxns[sortRxns] &&
            model1.mets == model1Ref.mets[sortMets]
    model1 = getModel(gm, "toyModel1")
    @test model1.S == model1Ref.S[sortMets, sortRxns]
    @test_throws ArgumentError getModel(gm, 4)
    @test_throws ArgumentError getModel(gm, "toyModel4")
end
