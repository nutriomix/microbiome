@testset "Basic functions" begin
    gm = GlobalModel(test_LP(),
                        ones(1, 3),
                        ["bm"],
                        ["model"])

    gm2 = GlobalModel(test_LP(),
                        ones(1, 3),
                        ["bm"],
                        ["model"])

    @test isequal(gm, gm2)

    gm2.modelIDs[1] = "mb"
    @test !isequal(gm, gm2)

    copyGm = copy(gm)
    @test isequal(gm, copyGm)

    copyGm.modelIDs[1] = "mb"
    @test isequal(gm, copyGm)

    gm = GlobalModel(test_LP(),
                        ones(1, 3),
                        ["bm"],
                        ["model"])

    deepcopyGm = deepcopy(gm)
    deepcopyGm.modelIDs[1] = "mb"
    @test !isequal(gm, deepcopyGm)

    ms = MetaSet([0.1 0.9], ["s1"; "s2"], ones(2), [], gm)
    deepcopyMs = deepcopy(ms)
    @test isequal(ms, deepcopyMs)
    ms.sampleIDs[1] = "s3"
    @test !isequal(ms, deepcopyMs)
end
