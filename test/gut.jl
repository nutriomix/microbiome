@testset "Apply diet" begin
    globalModel = buildGlobalModel(joinpath("data", "toyModelsList.txt"), "data")
    dietModel = applyDiet(globalModel, joinpath("data", "toyModelDiet.txt"))
    @test dietModel.coreModel.rxns[2:5] == ["EX_m1(e)";
                                                    "EX_m2(e)";
                                                    "EX_m3(e)";
                                                    "EX_m4(e)"]
    @test dietModel.coreModel.xl[2:5] == [-1; -0.05*100; 0; 0]
    @test dietModel.coreModel.xl[setdiff(1:13, 2:5)] == globalModel.coreModel.xl[setdiff(1:13, 2:5)]

    comModel = createCommunityReactions(globalModel)
    dietModel = applyDiet(comModel, joinpath("data", "toyModelDiet.txt"))
    @test dietModel.coreModel.rxns[13:16] == ["EX_m1(com)";
                                                "EX_m2(com)";
                                                "EX_m3(com)";
                                                "EX_m4(com)"]
    @test dietModel.coreModel.xl[13:16] == [-1; -0.05*100; 0; 0]
    dietModel = applyDiet(comModel, joinpath("data", "toyModelDiet2.txt"))
    @test dietModel.coreModel.xl[13:16] == [-1; -0.05*100; 0; 0]
    dietModel = deepcopy(comModel)
    applyDiet!(dietModel, joinpath("data", "toyModelDiet.txt"))
    @test dietModel.coreModel.xl[13:16] == [-1; -0.05*100; 0; 0]
    expModel = expandCommunityModel(comModel, [0.1; 0.2; 0.7])
    @test expModel.rxns[24:27] == ["EX_m1(com)";
                                   "EX_m2(com)";
                                   "EX_m3(com)";
                                   "EX_m4(com)"]
    dietExpModel = applyDiet(expModel, joinpath("data", "toyModelDiet.txt"))
    @test dietExpModel.xl[24:27] == [-1; -0.05*100; 0; 0]
end

@testset "MetaSet" begin
    ms = MetaSet(joinpath("data", "abundanceTable.csv"),
                    joinpath("data", "toyGlobalModel.mat"))
    @test ms isa MetaSet
    @test nSamples(ms) == 3
    @test isCommunityModel(ms.comModel)
    splitd = splitMetaSet(ms, 2)
    @test splitd[1] isa MetaSet
    @test splitd[2] isa MetaSet
    ms2 = mergeMetaSets(splitd)
    @test ms2 isa MetaSet
    @test isequal(ms, ms2)
    # @test_throws ErrorException mergeMetaSets(ms, ms)

    ms = MetaSet(joinpath("data", "abundanceTable.csv"),
                    joinpath("data", "toyCommunityModel.mat"))
    @test ms isa MetaSet

    cm = getModel(ms, 2)
    cm_ref = expandCommunityModel(ms.comModel, ms.modelAbundances[:, 2])
    isequal(cm, cm_ref)

    rxnAb = getReactionAbundances(ms)
    @test rxnAb[1,1] == ms.comModel.rxnPres[1,1]*ms.modelAbundances[1,1]+
                        ms.comModel.rxnPres[2,1]*ms.modelAbundances[2,1]+
                        ms.comModel.rxnPres[1,3]*ms.modelAbundances[3,1]
    @test rxnAb[15,2] == ms.comModel.rxnPres[1,15]*ms.modelAbundances[1,2]+
                        ms.comModel.rxnPres[2,15]*ms.modelAbundances[2,2]+
                        ms.comModel.rxnPres[3,15]*ms.modelAbundances[3,2]
end

@testset "Flux analysis" begin
    toyGlobalModel = loadGlobalModel(joinpath("data", "toyGlobalModel.mat"))
    toyComModel = loadGlobalModel(joinpath("data", "toyCommunityModel.mat"))

    # FBA
    ms = MetaSet(joinpath("data", "abundanceTable.csv"), joinpath("data", "toyGlobalModel.mat"))
    runFBA!(ms, GLPK.Optimizer)
    abundances2 = ms.modelAbundances[:, 2]
    expModel2 = expandCommunityModel(toyComModel, abundances2)
    excs = find_exchange_reactions(expModel2)
    @test ms.results.FBA["resIDs"] == expModel2.rxns[excs]
    optModel = COBREXA.flux_balance_analysis(expModel2, GLPK.Optimizer)
    x = JuMP.value.(JuMP.all_variables(optModel))
    @test value.(x[excs]) ≈ ms.results.FBA["fluxes"][:, 2]

    # including a diet does not modify MetaSet comModel
    runFBA!(ms, GLPK.Optimizer, dietFile=joinpath("data", "toyModelDiet.txt"))
    @test isequal(ms.comModel, toyComModel)

    # FVA
    runFVA!(ms, GLPK.Optimizer)
    @test ms.results.FVA["resIDs"] == expModel2.rxns[excs]
    eFluxes = COBREXA.flux_variability_analysis(expModel2, excs, GLPK.Optimizer)
    @test eFluxes ≈ [ms.results.FVA["min"][:, 2] ms.results.FVA["max"][:, 2]]

    runFVA!(ms, GLPK.Optimizer, reactions=["EX_m2(com)"])
    @test ms.results.FVA["resIDs"] == ["EX_m2(com)"]
    @test eFluxes[2, :] ≈ [ms.results.FVA["min"][:, 2] ms.results.FVA["max"][:, 2]][:] # this [:] really shouldn't be needed

    # # including a diet does not modify MetaSet comModel
    runFVA!(ms, GLPK.Optimizer, dietFile=joinpath("data", "toyModelDiet.txt"))
    @test isequal(ms.comModel, toyComModel)

    splitd = splitMetaSet(ms, 2)
    merged = mergeMetaSets(splitd)
    @test isequal(ms, merged)
    ms = MetaSet(joinpath("data", "abundanceTable.csv"), joinpath("data", "toyGlobalModel.mat"))
    splitd = splitMetaSet(ms, 2)
    runFBA!(splitd[1], GLPK.Optimizer)
    runFVA!(splitd[1], GLPK.Optimizer)
    merged = mergeMetaSets(splitd)
    @test isnothing(merged.results.FBA["fluxes"])
end

@testset "I/O results" begin
    ms = MetaSet(joinpath("data", "abundanceTable2.csv"),
                    joinpath("data", "toyCommunityModel.mat"))
    runFBA!(ms, GLPK.Optimizer)
    runFVA!(ms, GLPK.Optimizer)
    exportMetaSet("example", ms, filePrefix="test_")

    @test isdir("example")
    filesToTest = ["modelAbundances"; "FBA_objValues"; "FBA_fluxes"; "FVA_max";
                    "FVA_min"; "coverage"; "removedSamples"; "reactionAbundances"]
    for ft in filesToTest
        e = read(joinpath("example", "test_"*ft), String)
        t = read(joinpath("data", "output", ft), String)
        @test e == t
    end

    ms2 = importMetaSet("example", joinpath("data", "toyCommunityModel.mat"), filePrefix="test_")
    @test isequal(ms, ms2)
    ms2 = importMetaSet("example", joinpath("data", "toyGlobalModel.mat"), filePrefix="test_")
    @test isequal(ms, ms2)

    ms = MetaSet(joinpath("data", "abundanceTable.csv"),
                    joinpath("data", "toyCommunityModel.mat"))
    splitd = splitMetaSet(ms, 2)
    runFBA!(ms, GLPK.Optimizer)
    runFBA!(splitd[1], GLPK.Optimizer)
    runFBA!(splitd[2], GLPK.Optimizer)
    exportMetaSet("example", splitd[1], filePrefix="splitd1_")
    exportMetaSet("example", splitd[2], filePrefix="splitd2_")
    splitd1 = importMetaSet("example", joinpath("data", "toyCommunityModel.mat"), filePrefix="splitd1_")
    splitd2 = importMetaSet("example", joinpath("data", "toyCommunityModel.mat"), filePrefix="splitd2_")
    merged = mergeMetaSets(splitd1, splitd2)
    @test isequal(merged, ms)

    rm("example", recursive=true)
end
