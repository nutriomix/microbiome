@testset "Import/export GlobalModel" begin
    gm = buildGlobalModel(joinpath("data", "toyModelsList.txt"), "data")
    writeGlobalModel("test.mat", gm)
    @test isfile("test.mat")

    gm2 = loadGlobalModel("test.mat")
    @test gm2 isa GlobalModel
    @test isequal(gm, gm2)

    @test_throws ErrorException loadGlobalModel("test.mat", "modle")

    rm("test.mat")
end

@testset "Importing abundance data" begin
    modelIDs = ["toyModel1"; "toyModel2"; "toyModel3"]
    (modelAbundances, sampleIDs, coverage, removedSamples) =
        mapToModels(joinpath("data", "abundanceTable.csv"), ',', modelIDs)
    @test modelAbundances == [0.5 0.25 0.8; 0.25 0.25 0.1; 0.25 0.5 0.1]
    @test sampleIDs == ["sample1"; "sample2"; "sample3"]
    @test coverage == [0.8; 0.8; 1]
    @test isempty(removedSamples)

    (modelAbundances, sampleIDs, coverage, removedSamples) =
        mapToModels(joinpath("data", "abundanceTable2.csv"), ',', modelIDs)
    @test vec(modelAbundances) == [0.5; 0; 0.5]
    @test sampleIDs == ["sample1"]
    @test coverage == [0.4]
    @test removedSamples == ["sample2"]
end
