const VT = Union{Array{Float64,1}, SparseVector{Float64,Int64}}
const MT = Union{AbstractMatrix, SparseMatrixCSC{Float64,Int64}}
const ST = Union{Array{String,1}, SparseVector{String,Int64}}
const BVT = Union{AbstractVector, SparseVector{Bool,Int64}}
const BMT = Union{AbstractArray, SparseMatrixCSC{Bool,Int64}}

"""
    mutable struct GlobalModel

A GlobalModel stores a collection of metabolic models.

The field `coreModel` contains the essential information of each unique reaction.
`rxnPres` is a Boolean matrix whose rows correspond to the individual models and
columns to reactions in `coreModel`. A true entry means that the corresponding
model possesses the corresponding reaction.
`biomassRxns` contains the reaction ids of models' biomass reactions.
`modelsIDs` are the names of the models.

See also: [`buildGlobalModel`](@ref)

# Fields
```
coreModel :: COBREXA.CoreModel
rxnPres ::
biomassRxns ::
modelIDs ::
```

# Example
```
model = loadGlobalModel("model_location")
```
"""
mutable struct GlobalModel
    coreModel ::CoreModel
    rxnPres
    biomassRxns
    modelIDs

    function GlobalModel(
        coreModel ::CoreModel,
        rxnPres,
        biomassRxns,
        modelIDs)

        (nMets, nRxns) = size(coreModel.S)
        nModels = length(modelIDs)

        size(rxnPres) == (nModels, nRxns) || throw(DimensionMismatch("`rxnPres` shape doesn't match with `nModels` and `nRxns`"))

        new(coreModel, rxnPres, biomassRxns, modelIDs)
    end
end

"""
    mutable struct Results

Stores analysis results inside a MetaSet.

See also: [`MetaSet`](@ref)

# Fields
```
FBA :: Dict{String, Any}
FVA :: Dict{String, Any}
```
"""
mutable struct Results
    FBA::Dict{String, Any}
    FVA::Dict{String, Any}

    function Results()
        new(Dict("fluxes"=>nothing, "resIDs"=>nothing, "objValues"=>nothing),
                Dict("min"=>nothing, "max"=>nothing, "resIDs"=>nothing))
    end
end

"""
    mutable struct MetaSet

MetaSet acts as a scaffold for creating and analysing microbiome models from a
metagenomics data set.

`modelAbundances` is an abundance matrix that gives the relative abundances for
the submodels in `comModel´.
`sampleIDs` is a vector of ids corresponding to the columns of `modelAbundances`
`coverage` is generated upon importing the data, and gives for each sample the
total abundance that was successfully mapped to the submodels.
`removedSamples` contains the IDs of samples that were ignored upon creation due
extremely low coverage.
`comModel` is GlobalModel object representing the model collection that is the base
of the sample-specific community models. It is a "community model" containing a
pseudo-submodel representing the shared extracellular environment (see
[`createCommunityReactions`](@ref)).

# Fields
```
modelAbundances
sampleIDs
coverage
removedSamples
comModel :: GlobalModel
results :: Results
```
"""
mutable struct MetaSet
    modelAbundances
    sampleIDs
    coverage
    removedSamples
    comModel::GlobalModel
    results::Results

    function MetaSet(
        abundanceTable::String,
        globalModelPath::String;
        delimiter::Char=',',
        varName::String="model"
        )

        model = loadGlobalModel(globalModelPath, varName)

        MetaSet(abundanceTable, model, delimiter=delimiter)
    end

    function MetaSet(
        abundanceTable::String,
        globalModel::GlobalModel;
        delimiter::Char=','
        )

        if isCommunityModel(globalModel)
            (modelAbundances, sampleIDs, coverage, removedSamples) = mapToModels(abundanceTable,
                                                                delimiter,
                                                                globalModel.modelIDs[1:end-1])
        else
            (modelAbundances, sampleIDs, coverage, removedSamples) = mapToModels(abundanceTable,
                                                                delimiter,
                                                                globalModel.modelIDs)
        end

        MetaSet(modelAbundances, sampleIDs, coverage, removedSamples, globalModel)
    end

    function MetaSet(
        modelAbundances,
        sampleIDs,
        coverage,
        removedSamples,
        globalModel::GlobalModel
        )

        if !isCommunityModel(globalModel)
            globalModel = createCommunityReactions(globalModel)
        end

        size(modelAbundances, 2) == length(sampleIDs) || throw(DimensionMismatch("`modelAbundances` does not match with `sampleIDs`"))
        length(sampleIDs) == length(coverage) || throw(DimensionMismatch("`sampleIDs` does not match with `coverage`"))
        size(modelAbundances, 1) == nModels(globalModel)-1 || throw(DimensionMismatch("`modelAbundances` does not match with `globalModel`"))

        new(modelAbundances, sampleIDs, coverage, removedSamples, globalModel, Results())
    end
end
