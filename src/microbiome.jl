module microbiome

using Logging
using SparseArrays
using DelimitedFiles
using LinearAlgebra
using JuMP
using MAT

using COBREXA

include("types.jl")
include("reconstruction.jl")
include("utilities.jl")
include("communityModeling.jl")
include("gut.jl")
include("io.jl")

export GlobalModel, MetaSet,                                    # types
       join, emptyGlobalModel, buildGlobalModel, nModels,       # reconstruction
       remove_reactions, add_reactions, removeModels, mergeModels,
       getModel, find_exchange_reaction_metabolites,
       createCommunityReactions, isCommunityModel,              # community
       expandCommunityModel, communityFBA, communityFVA,
       applyDiet!, applyDiet, runFBA!, runFVA!,                 # gut
       nSamples, splitMetaSet, mergeMetaSets,
       getReactionAbundances,
       writeGlobalModel, loadGlobalModel, mapToModels,          # io
       exportMetaSet, importMetaSet

end
