"""
    applyDiet!(
        model::CoreModel,
        dietFile::String;
        delimiter::Char='\t'
        header::Bool=true,
        adaptVMH::Bool=true
    )

Apply a "diet" read from a diet file on a gut microbiome model by re-setting
exchange bounds.

`model` can be a `COBREXA.CoreModel` or a `GlobalModel`. This function was designed
to work with diets from the Virtual Metabolic Human [1] but will work with
any correctly formatted file. By default, diets are adapted to ensure viability
with ad hoc corrections used in previous pipelines [2].

1. Noronha, Alberto, et al. "The Virtual Metabolic Human database: integrating
human and gut microbiome metabolism with nutrition and disease." Nucleic acids
research 47.D1 (2019): D614-D624.

2. Baldini, Federico, et al. "Parkinson’s disease-associated alterations of the gut
microbiome predict disease-relevant changes in metabolic functions." BMC biology 18.1 (2020): 1-21.

"""
function applyDiet!(model::CoreModel, dietFile::String; delimiter::Char='\t', header::Bool=true, adaptVMH::Bool=true)
    (IDs, bounds) = _readDiet(dietFile, delimiter, header)

    if adaptVMH
        (IDs, bounds) = _modifyVMHdiet(IDs, bounds)
    end

    excs = find_exchange_reactions(model, exclude_biomass=true)
    change_bounds!(model, excs, lower=zeros(length(excs)))
    if endswith(model.rxns[excs[1]], "(com)")
        IDs = "EX_" .* IDs .* "(com)"
    else
        IDs = "EX_" .* IDs .* "(e)"
    end
    idxs = indexin(IDs, reactions(model))
    ok_idxs = .! isnothing.(idxs)
    change_bounds!(model, Vector{Int}(idxs[ok_idxs]), lower=bounds[ok_idxs])
end

"""
    applyDiet(
        model::CoreModel,
        dietFile::String;
        delimiter::Char='\t'
        header::Bool=true,
        adaptVMH::Bool=true
    )

"""
function applyDiet(model::CoreModel, dietFile::String; delimiter::Char='\t', header::Bool=true, adaptVMH::Bool=true)
    newModel = deepcopy(model)
    applyDiet!(newModel, dietFile, header=header)
    return newModel
end

"""
    applyDiet!(
        model::GlobalModel,
        dietFile::String;
        delimiter::Char='\t',
        header::Bool=true,
        adaptVMH::Bool=true
    )
"""
function applyDiet!(model::GlobalModel, dietFile::String; delimiter::Char='\t', header::Bool=true, adaptVMH::Bool=true)
    applyDiet!(model.coreModel, dietFile, header=header)
end

"""
    applyDiet(
        model::GlobalModel,
        dietFile::String;
        delimiter::Char='\t',
        header::Bool=true,
        adaptVMH::Bool=true
    )
"""
function applyDiet(model::GlobalModel, dietFile::String; delimiter::Char='\t', header::Bool=true, adaptVMH::Bool=true)
    newModel = deepcopy(model)
    applyDiet!(newModel.coreModel, dietFile, header=header)
    return newModel
end

"""
Ad hoc modification to VMH diets.
"""
function _modifyVMHdiet(IDs, bounds)

    essentialMets = ["12dgr180"; "26dap_M"; "2dmmq8";
        "2obut"; "3mop"; "4abz"; "4hbz"; "ac";
        "acgam"; "acmana"; "acnam"; "ade"; "adn";
        "adocbl"; "adpcbl"; "ala_D"; "ala_L"; "amet";
        "amp"; "arab_D"; "arab_L"; "arg_L"; "asn_L";
        "btn"; "ca2"; "cbl1"; "cgly"; "chor";
        "chsterol"; "cit"; "cl"; "cobalt2"; "csn";
        "cu2"; "cys_L"; "cytd"; "dad_2"; "dcyt";
        "ddca"; "dgsn"; "fald"; "fe2"; "fe3";
        "for"; "gal"; "glc_D"; "gln_L";
        "glu_L"; "gly"; "glyc"; "glyc3p"; "gsn";
        "gthox"; "gthrd"; "gua"; "h"; "h2o";
        "h2s"; "his_L"; "hxan"; "ile_L"; "k";
        "lanost"; "leu_L"; "lys_L"; "malt";
        "met_L"; "mg2"; "mn2"; "mqn7"; "mqn8";
        "nac"; "ncam"; "nmn"; "no2"; "ocdca";
        "ocdcea"; "orn"; "phe_L"; "pheme"; "pi";
        "pnto_R"; "pro_L"; "ptrc"; "pydx";
        "pydxn"; "q8"; "rib_D"; "ribflv";
        "ser_L"; "sheme"; "so4"; "spmd"; "thm";
        "thr_L"; "thymd"; "trp_L"; "ttdca"; "tyr_L";
        "ura"; "val_L"; "xan"; "xyl_D"; "zn2"]

    toAdd = findall([!(met in IDs) for met in essentialMets])
    IDs = [IDs; essentialMets[toAdd]]
    bounds = [bounds; fill(-0.1, length(toAdd))]
    unMapped = ["asn_L";"gln_L";"crn";"elaid";
        "hdcea";"dlnlcg";"adrn";"hco3";"sprm";
        "carn";"7thf";"Lcystin";"hista";"orn";
        "ptrc";"creat";"cytd";"so4"]
    toAdd = findall([!(met in IDs) for met in unMapped])
    IDs = [IDs; unMapped[toAdd]]
    bounds = [bounds; fill(-50, length(toAdd))]

    if !("chol" in IDs)
        IDs = [IDs; "chol"]
        bounds = [bounds; -41.251]
    end

    tmpInd = findfirst(isequal("fol"), IDs)
    if isnothing(tmpInd)
        IDs = [IDs; "fol"]
        bounds = [bounds; -1]
    elseif bounds[tmpInd] > -1
        bounds[tmpInd] = -1
    end

    microN = ["adocbl";"vitd2";"vitd3";
        "psyl";"gum";"bglc";"phyQ";"fol";
        "5mthf";"q10";"retinol_9_cis";"pydxn";
        "pydam";"pydx";"pheme";"ribflv";"thm";
        "avite1";"pnto_R";"na1";"cl";"k";
        "pi";"zn2";"cu2";"m2"]
    toChange = findall([((IDs[ind] in microN) && (bounds[ind] >= -0.1)) for ind in 1:length(IDs)])
    if !isempty(toChange)
        bounds[toChange] = bounds[toChange].*100
    end

    return (IDs, bounds)
end

"""
Auxiliary function for `applyDiet` to read a diet file.
"""
function _readDiet(file::String, delimiter::Char, header::Bool)
    diet = readdlm(file, delimiter, String)
    (i, j) = size(diet)
    j==2 || error("`dietFile` should have two columns")
    if header
        diet = diet[2:end, :]
    end
    IDs = diet[:, 1]
    IDs = replace.(replace.(IDs, "EX_"=>""), "(e)"=>"")
    bounds = -parse.(Float64, diet[:, 2])

    return (IDs, bounds)
end

"""
    runFBA!(
        mSet::MetaSet,
        optimizer;
        scalingStrat=:abScale,
        dietFile::String="",
        dietFileHeader::Bool=true
    )

Run `communityFBA` on all samples in a `MetaSet` and save results within.

NB: existing results will be overwritten.

See also: [`MetaSet`](@ref), [`communityFBA`](@ref)

"""
function runFBA!(mSet::MetaSet, optimizer; scalingStrat=:abScale, dietFile::String="",
                    dietFileHeader::Bool=true)
    if !isempty(dietFile)
        comModel = applyDiet(mSet.comModel, dietFile, header=dietFileHeader)
    else
        comModel = mSet.comModel
    end
    nSamples = length(mSet.sampleIDs)
    excs = find_exchange_reactions(comModel.coreModel)
    objValues = zeros(nSamples)
    fluxes = spzeros(length(excs), nSamples)
    resIDs = comModel.coreModel.rxns[excs]
    for i in 1:nSamples
        (objValues[i], fluxes[:, i]) = communityFBA(comModel,
                                        mSet.modelAbundances[:, i],
                                        optimizer, scalingStrat=scalingStrat)
    end
    mSet.results.FBA["objValues"] = objValues
    mSet.results.FBA["fluxes"] = fluxes
    mSet.results.FBA["resIDs"] = resIDs
end

"""
    runFVA!(
        mSet::MetaSet,
        optimizer;
        reactions::Vector{String}=Vector{String}([]),
        scalingStrat=:abScale,
        dietFile::String="",
        dietFileHeader::Bool=true
    )

Run `communityFVA` on all samples in a `MetaSet` and save results within.

NB: existing results will be overwritten.

See also: [`MetaSet`](@ref), [`communityFVA`](@ref)

"""
function runFVA!(mSet::MetaSet, optimizer; reactions::Vector{String}=Vector{String}([]), gamma::AbstractFloat=1.0,
                    scalingStrat=:abScale, dietFile::String="", dietFileHeader::Bool=true)
    if !isempty(dietFile)
        comModel = applyDiet(mSet.comModel, dietFile, header=dietFileHeader)
    else
        comModel = mSet.comModel
    end

    if isempty(reactions)
        excs = find_exchange_reactions(comModel.coreModel)
        resIDs = comModel.coreModel.rxns[excs]
    else
        resIDs = reactions
    end

    nSamples = length(mSet.sampleIDs)
    min = spzeros(length(resIDs), nSamples)
    max = spzeros(length(resIDs), nSamples)
    for i in 1:nSamples
        if isempty(reactions)
            fluxes = communityFVA(comModel, mSet.modelAbundances[:, i],
                            optimizer, gamma=gamma, scalingStrat=scalingStrat)
        else
            fluxes = communityFVA(comModel, mSet.modelAbundances[:, i],
                            reactions, optimizer, gamma=gamma, scalingStrat=scalingStrat)
        end
        min[:, i] = fluxes[:, 1]
        max[:, i] = fluxes[:, 2]
    end
    mSet.results.FVA["min"] = min
    mSet.results.FVA["max"] = max
    mSet.results.FVA["resIDs"] = resIDs
end

"""
    nSamples(
        mSet::MetaSet
    )

Return the number of samples in a MetaSet.

"""
function nSamples(mSet::MetaSet)
    return length(mSet.sampleIDs)
end

"""
    splitMetaSet(
        mSet::MetaSet,
        n::Int
    )

Split a MetaSet by samples.

"""
function splitMetaSet(mSet::MetaSet, n::Int)
    nSamps = nSamples(mSet)
    if n>nSamps
        n = nSamps
    end
    fullSize = ceil(Int, nSamps/n)
    i = 1
    round = 1
    subMsets = Vector{MetaSet}(undef, n)
    while i<=nSamps
        intval = i:min(i+fullSize-1, nSamps)
        modelAbs = mSet.modelAbundances[:, intval]
        sampleIDs = mSet.sampleIDs[intval]
        coverage = mSet.coverage[intval]
        removedSamples = mSet.removedSamples
        model = mSet.comModel
        newMset = MetaSet(modelAbs, sampleIDs, coverage, removedSamples, model)
        if !isnothing(mSet.results.FBA["fluxes"])
            newMset.results.FBA["fluxes"] = mSet.results.FBA["fluxes"][:, intval]
            newMset.results.FBA["objValues"] = mSet.results.FBA["objValues"][intval]
            newMset.results.FBA["resIDs"] = mSet.results.FBA["resIDs"]
        end
        if !isnothing(mSet.results.FVA["min"])
            newMset.results.FVA["min"] = mSet.results.FVA["min"][:, intval]
            newMset.results.FVA["max"] = mSet.results.FVA["max"][:, intval]
            newMset.results.FVA["resIDs"] = mSet.results.FVA["resIDs"]
        end
        subMsets[round] = newMset

        i = i + fullSize
        round += 1
    end

    return subMsets
end

"""
    mergeMetaSets(
        mSet1::MetaSet,
        mSet2::MetaSe
    )

Merge MetaSets split by `splitMetaSet`.

"""
function mergeMetaSets(mSet1::MetaSet, mSet2::MetaSet)
    isempty(intersect(mSet1.sampleIDs, mSet2.sampleIDs)) || @error "Sample IDs will conflict!"
    isequal(mSet1.comModel, mSet2.comModel) || @error "GlobalModels do not match!"

    modelAbs = [mSet1.modelAbundances mSet2.modelAbundances]
    sampleIDs = [mSet1.sampleIDs; mSet2.sampleIDs]
    coverage = [mSet1.coverage; mSet2.coverage]
    removedSamples = mSet1.removedSamples
    newMset = MetaSet(modelAbs, sampleIDs, coverage, removedSamples, mSet1.comModel)
    if !isnothing(mSet1.results.FBA["fluxes"]) && !isnothing(mSet2.results.FBA["fluxes"])
        newMset.results.FBA["fluxes"] = [mSet1.results.FBA["fluxes"] mSet2.results.FBA["fluxes"]]
        newMset.results.FBA["objValues"] = [mSet1.results.FBA["objValues"]; mSet2.results.FBA["objValues"]]
        newMset.results.FBA["resIDs"] = mSet1.results.FBA["resIDs"]
    elseif !isnothing(mSet1.results.FBA["fluxes"]) || !isnothing(mSet2.results.FBA["fluxes"])
        @warn "Partial FBA results ignored"
    end
    if !isnothing(mSet1.results.FVA["min"]) && !isnothing(mSet2.results.FVA["min"])
        newMset.results.FVA["min"] = [mSet1.results.FVA["min"] mSet2.results.FVA["min"]]
        newMset.results.FVA["max"] = [mSet1.results.FVA["max"] mSet2.results.FVA["max"]]
        newMset.results.FVA["resIDs"] = mSet1.results.FVA["resIDs"]
    elseif !isnothing(mSet1.results.FVA["min"]) || !isnothing(mSet2.results.FVA["min"])
        @warn "Partial FVA results ignored"
    end

    return newMset
end

"""
    mergeMetaSets(
        mSets::Array{MetaSet, 1}
    )

"""
function mergeMetaSets(mSets::Array{MetaSet, 1})
    newMset = mSets[1]
    for i=2:length(mSets)
        newMset = mergeMetaSets(newMset, mSets[i])
    end

    return newMset
end

"""
    getModel(
        ms::MetaSet,
        nSample::Int;
        scalingStrat=:abScale
    )

Get the community model specified by the abundance vector at sample `nSample`
as a `COBREXA.CoreModel`.

See also: [`MetaSet`](@ref), [`expandCommunityModel`](@ref)

"""
function getModel(ms::MetaSet, nSample::Int; scalingStrat=:abScale)
    nSample > nSamples(ms) && error("`nSample` exceeds the number of samples in `ms`.")

    return expandCommunityModel(ms.comModel, ms.modelAbundances[:, nSample], scalingStrat=scalingStrat)
end

"""
    getReactionAbundances(
        ms::MetaSet
    )

Get an abundance table in terms of reactions.

Generates an abundance table that tells for each sample in `ms` how common each reaction is,
ie, if the entry is 0.4, it means organisms comprising 0.4 of the community have that reaction.
Rows correspond to reactions and columns to samples.
Note that reaction list comes from the `GlobalModel` and thus it is possible to have
zero rows (reaction not present in any of the samples).

"""
function getReactionAbundances(ms::MetaSet)
    indCom = findfirst(isequal("community"), ms.comModel.modelIDs)
    return ms.comModel.rxnPres[1:end .!= indCom, :]' * ms.modelAbundances
end
