"""
    writeGlobalModel(
        filePath::String,
        model::GlobalModel,
        varName::String="model"
    )

Write a GlobalModel to a MAT file.

"""
function writeGlobalModel(filePath::String, model::GlobalModel, varName::String="model")
    modelToWrite = Dict("coreModel"=>_convertToExportable(model.coreModel),
                        "rxnPres"=>model.rxnPres,
                        "biomassRxns"=>model.biomassRxns,
                        "modelIDs"=>model.modelIDs)
    matwrite(filePath, Dict(varName => modelToWrite))
end

"""
    loadGlobalModel(
        filePath::String,
        varName::String="model"
    )

Load a GlobalModel from a MAT file.

"""
function loadGlobalModel(filePath::String, varName::String="model")
    vars = matread(filePath)

    if haskey(vars, varName)

        model = vars[varName]
        modelKeys = ["coreModel"; "rxnPres"; "biomassRxns"; "modelIDs"]

        for key in modelKeys
            if !(key in keys(model))
                error("No variable $key found in the MAT file.")
            end
        end

        coreModel = convert(CoreModel, COBREXA.MATModel(model["coreModel"]))

        rxnPres = model["rxnPres"]
        biomassRxns = model["biomassRxns"]
        modelIDs = model["modelIDs"]

        return GlobalModel(coreModel,
                           rxnPres,
                           biomassRxns,
                           modelIDs)
    else
        error("Variable `varName` does not exist in the specified MAT file.")
    end
end

"""
    mapToModels(
        abundanceTable,
        delimiter,
        modelIDs
    )

Map an abundance table from an input file onto a list of model IDs.

Primarily an auxiliary function for creating `MetaSet`s.

"""
function mapToModels(abundanceTable, delimiter, modelIDs)
    (orgNames, abundances, sampleIDs) = _readAbTable(abundanceTable, delimiter)
    foundModel = [name in modelIDs for name in orgNames]
    coverage = vec(round.(sum(abundances[foundModel, :], dims=1), sigdigits=3))
    abInd = [findfirst(ID .== orgNames) for ID in modelIDs]
    foundAb = .!isnothing.(abInd)
    modelAbundances = spzeros(length(modelIDs), length(sampleIDs))
    modelAbundances[foundAb, :] = abundances[abInd[foundAb], :]
    coverage = vec(round.(sum(modelAbundances, dims=1), sigdigits=3))
    lowCov = coverage .< 0.001
    removedSamples = sampleIDs[lowCov]
    if any(lowCov)
        !all(lowCov) || @error "Coverage is very low or zero for all samples."
        @warn "One or more samples has very low or zero coverage: they will be removed."
        sampleIDs = sampleIDs[.!lowCov]
        modelAbundances = modelAbundances[:, .!lowCov]
        coverage = coverage[.!lowCov]
    end
    modelAbundances = modelAbundances ./ sum(modelAbundances, dims=1)

    return return (modelAbundances, sampleIDs, coverage, removedSamples)
end

"""
Auxiliary function for `mapToModels` and `importMetaSet`.

Reads a delimited file and extracts names of organisms, abundances, and sample
names.

"""
function _readAbTable(abundanceTable, delimiter)
    (table, colNames) = readdlm(abundanceTable, delimiter, header=true)
    sampleIDs = vec(colNames[2:end])
    orgNames = vec(table[:, 1])
    abundances = Array{Float64, 2}(table[:,2:end])
    all(abs.(sum(abundances, dims=1) .- 1) .< 0.0001) || @warn "Abundances from $abundanceTable do not sum to 1 for all samples."
    return (orgNames, abundances, sampleIDs)
end

"""
    exportMetaSet(
        folderPath::String,
        mSet::MetaSet;
        filePrefix::String="",
        delimiter::Char=','
    )

Export a MetaSet and possible results within into a structured folder.

Model abundances, mapping data, and any results will be written as text files
into the specified folder.
By specifying a `filePrefix`, you can separate different results within one folder
(eg analyses run with different diets).

See also: [`MetaSet`](@ref)

"""
function exportMetaSet(folderPath::String, mSet::MetaSet; filePrefix::String="",
                            delimiter::Char=',')
    isdir(folderPath) || mkdir(folderPath)

    writedlm(joinpath(folderPath, filePrefix*"modelAbundances"),
                ["X" reshape(mSet.sampleIDs, 1, length(mSet.sampleIDs));
                mSet.comModel.modelIDs[1:end-1] Matrix(mSet.modelAbundances)],
                delimiter)
    writedlm(joinpath(folderPath, filePrefix*"coverage"),
                [mSet.sampleIDs mSet.coverage],
                delimiter)
    if length(mSet.removedSamples) > 0
        writedlm(joinpath(folderPath, filePrefix*"removedSamples"),
                    [mSet.removedSamples])
    end
    if !isnothing(mSet.results.FBA["fluxes"])
        writedlm(joinpath(folderPath, filePrefix*"FBA_objValues"),
                    [mSet.sampleIDs mSet.results.FBA["objValues"]], delimiter)
        writedlm(joinpath(folderPath, filePrefix*"FBA_fluxes"),
                    ["X" reshape(mSet.sampleIDs, 1, length(mSet.sampleIDs));
                    mSet.results.FBA["resIDs"] Matrix(mSet.results.FBA["fluxes"])],
                    delimiter)
    end
    if !isnothing(mSet.results.FVA["min"])
        writedlm(joinpath(folderPath, filePrefix*"FVA_min"),
                    ["X" reshape(mSet.sampleIDs, 1, length(mSet.sampleIDs));
                    mSet.results.FVA["resIDs"] Matrix(mSet.results.FVA["min"])],
                    delimiter)
        writedlm(joinpath(folderPath, filePrefix*"FVA_max"),
                    ["X" reshape(mSet.sampleIDs, 1, length(mSet.sampleIDs));
                    mSet.results.FVA["resIDs"] Matrix(mSet.results.FVA["max"])],
                    delimiter)
    end
    writedlm(joinpath(folderPath, filePrefix*"reactionAbundances"),
                ["X" reshape(mSet.sampleIDs, 1, length(mSet.sampleIDs));
                [mSet.comModel.coreModel.rxns Array(getReactionAbundances(mSet))]],
                delimiter)
end

"""
    importMetaSet(
        folderPath::String,
        modelFile::String;
        filePrefix::String="",
        delimiter::Char=',',
        varName::String="model"
    )

Import a MetaSet from a structured folder.

Reads the contents of a folder created by `exportMetaSet` and creates a `MetaSet`
object.

See also: [`exportMetaSet`](@ref)

"""
function importMetaSet(folderPath::String, modelFile::String; filePrefix::String="",
                                                            delimiter::Char=',', varName::String="model")
    isdir(folderPath) || @error "No folder named `folderPath` found!"

    if isfile(joinpath(folderPath, filePrefix*"modelAbundances"))
        (tableModelIDs, abundances, tableSampleIDs) =
            _readAbTable(joinpath(folderPath, filePrefix*"modelAbundances"), delimiter)
    else
        @error "Abundance table not found!"
    end

    if isfile(joinpath(folderPath, filePrefix*"coverage"))
        coverage = readdlm(joinpath(folderPath, filePrefix*"coverage"), delimiter, header=false)
    else
        @error "Coverage not found!"
    end

    if isfile(joinpath(folderPath, filePrefix*"removedSamples"))
        removed = readdlm(joinpath(folderPath, filePrefix*"removedSamples"), header=false)[:]
    else
        removed = []
    end

    model = loadGlobalModel(modelFile, varName)
    if isCommunityModel(model)
        tableModelIDs == model.modelIDs[1:end-1] || @error "Model does not match the MetaSet!"
    else
        tableModelIDs == model.modelIDs[1:end] || @error "Model does not match the MetaSet!"
    end

    mSet = MetaSet(abundances, tableSampleIDs, coverage[:, 2], removed, model)

    if isfile(joinpath(folderPath, filePrefix*"FBA_fluxes"))
        FBAflux = readdlm(joinpath(folderPath, filePrefix*"FBA_fluxes"), delimiter, header=false)
        FBAobj = readdlm(joinpath(folderPath, filePrefix*"FBA_objValues"), delimiter, header=false)
        mSet.results.FBA["fluxes"] = sparse(Array{Float64, 2}(FBAflux[2:end,2:end]))
        mSet.results.FBA["objValues"] = Array{Float64, 1}(FBAobj[:, 2])
        mSet.results.FBA["resIDs"] = Array{String, 1}(FBAflux[2:end, 1])
    end

    if isfile(joinpath(folderPath, filePrefix*"FVA_min"))
        FVAmin = readdlm(joinpath(folderPath, filePrefix*"FVA_min"), delimiter, header=false)
        FVAmax = readdlm(joinpath(folderPath, filePrefix*"FVA_max"), delimiter, header=false)
        mSet.results.FVA["min"] = sparse(Array{Float64, 2}(FVAmin[2:end,2:end]))
        mSet.results.FVA["max"] = sparse(Array{Float64, 2}(FVAmax[2:end,2:end]))
        mSet.results.FVA["resIDs"] = Array{String, 1}(FVAmin[2:end, 1])
    end

    return mSet
end

"""
Auxiliary function for `writeGlobalModel`.

"""
function _convertToExportable(model::CoreModel)
    return Dict("S"    => model.S,
                "b"    => Array(model.b),
                "c"    => Array(model.c),
                "ub"   => Array(model.xu),
                "lb"   => Array(model.xl),
                "rxns" => model.rxns,
                "mets" => model.mets)
end
