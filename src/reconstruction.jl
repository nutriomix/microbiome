"""
    join(
        gm::GlobalModel,
        lm::CoreModel,
        biomassRxn::String,
        modelIDd::String
    )

Add a CoreModel to a GlobalModel.

"""
function join(gm::GlobalModel, lm::CoreModel, biomassRxn::String, modelID::String)

    (coreModel, newReactions, newMets) = add_reactions(gm.coreModel, lm, check_consistency=true)

    currRxnsBool = Bool[(rxn in lm.rxns) for rxn in coreModel.rxns]
    rxnPres = hcat(gm.rxnPres, spzeros(Bool, size(gm.rxnPres)[1], length(newReactions)))
    rxnPres = vcat(rxnPres, sparse(transpose(currRxnsBool)))

    biomassRxns = [gm.biomassRxns; biomassRxn]
    modelIDs = [gm.modelIDs; modelID]
    return GlobalModel(coreModel, rxnPres, biomassRxns, modelIDs)
end

"""
    emptyGlobalModel()

Create an empty GlobalModel.

"""
function emptyGlobalModel()
    emptyFloatArray = Array{Float64}(undef, 0)
    emptyStringArray = Array{String}(undef, 0)
    coreModel = CoreModel(Array{Float64}(undef, 0, 0), emptyFloatArray,
                              emptyFloatArray, emptyFloatArray, emptyFloatArray,
                              emptyStringArray, emptyStringArray)

    rxnPres = spzeros(Bool, 0, 0)
    biomassRxns = Array{String}(undef, 0)
    modelIDs = Array{String}(undef, 0)

    return GlobalModel(coreModel, rxnPres, biomassRxns, modelIDs)
end

"""
    buildGlobalModel(
        modelsList::AbstractArray,
        pathToModels::String,
        varName::String="model"
    )

Build a GlobalModel from a collection of individual models.

`modelsList` is an array of strings, each a filename in `pathToModels`. Any
format compatible with COBREXA should work but note that mixing formats may
create issues.
`pathToModels` is the path to the folder containing the models.

Model IDs are created from file names.

See also: [`GlobalModel`](@ref)

# Example
```
modelsList = ["toyModel1.mat"; "toyModel2.mat"]
pathToModels = joinpath("test/data")
globalModel = buildGlobalModel(modelsList, pathToModels)
```

"""
function buildGlobalModel(modelsList::AbstractArray, pathToModels::String)
    fileFound = [isfile(joinpath(pathToModels, fileN)) for fileN in modelsList]
    all(fileFound) || @warn "The following files were not found and will be ignored"
    for fileN in modelsList[.!fileFound]
        @warn "$fileN"
    end
    modelsList = modelsList[fileFound]
    nModels = length(modelsList)

    gm = emptyGlobalModel()

    # iterate through the models
    for iModel in 1:nModels
        # @info "Processing model $iModel"
        currFileN = modelsList[iModel]
        currModel = load_model(CoreModel, joinpath(pathToModels, currFileN))
        biomassRxn = currModel.rxns[findfirst(isequal(1), currModel.c)]
        modelID = replace(currFileN, ".mat"=>"")

        gm = join(gm, currModel, biomassRxn, modelID)
    end

    sort!(gm)
    return gm
end

"""
    buildGlobalModel(
        modelsListFile::String,
        pathToModels::String
    )

Give `modelsList` as a text file at `modelsListFile` with one model filename
per row.

"""
function buildGlobalModel(modelsListFile::String, pathToModels::String)
    modelsList = readdlm(modelsListFile)
    (tmpI, tmpJ) = size(modelsList)
    (tmpI > 0) & (tmpJ == 1) || error("`modelsListFile` should have one model name per line")

    return buildGlobalModel(modelsList[:], pathToModels)
end

"""
    nModels(
        m::GlobalModel
    )

Get the number of individual models `m`contains.

Note that a pseudomodel created by [`createCommunityReactions`](@ref) will be included.

"""
function nModels(m::GlobalModel)
    return length(m.modelIDs)
end

"""
    remove_reactions(
        model::GlobalModel,
        rxn::Union{String, Integer};
        removeBiomass=false
    )

Remove reaction(s) from a GlobalModel based on reaction ID or index in
`model.coreModel.rxns`.

Amounts to removing `rxns` from all models inside. By default, reactions
designated as biomass reactions in `model.biomassRxns` will not be removed and
a warning is issued.

"""
function COBREXA.remove_reactions(model::GlobalModel, rxn::Union{String, Integer}; removeBiomass=false)
    return remove_reactions(model, [rxn], removeBiomass=removeBiomass)
end

"""
    remove_reactions(
        model::GlobalModel,
        rxns::Array{String, 1};
        removeBiomass=false
    )

"""
function COBREXA.remove_reactions(model::GlobalModel, rxns::Array{String, 1}; removeBiomass=false)
    rxnIndices = [findfirst(isequal(name), model.coreModel.rxns) for name in intersect(rxns, model.coreModel.rxns)]
    if isempty(rxnIndices)
        return model
    else
        return remove_reactions(model, rxnIndices, removeBiomass=removeBiomass)
    end
end

"""
    remove_reactions(
        model::GlobalModel,
        rxns::Array{Int64, 1};
        removeBiomass=false
    )

"""
function COBREXA.remove_reactions(model::GlobalModel, rxns::Array{Int64, 1}; removeBiomass=false)
    notBiom = [!(name in model.biomassRxns) for name in model.coreModel.rxns[rxns]]
    if !all(notBiom) && !removeBiomass
        @warn "You are trying to remove a biomass reaction."
        rxns = rxns[notBiom]
    end
    newLModel = remove_reactions_with_metabolites(model.coreModel, rxns)
    newRxnPres = model.rxnPres[:, [!(i in rxns) for i in 1:n_reactions(model.coreModel)]]

    return GlobalModel(newLModel, newRxnPres, copy(model.biomassRxns), copy(model.modelIDs))
end

"""
    add_reactions(
        gm::GlobalModel,
        Sp::M,
        b::V,
        c::V,
        xl::BV,
        xu::BV,
        rxns::K,
        mets::K,
        rxnPres::B
    ) where {M<:MT,V<:VT,BV<:VT,K<:ST,B<:BMT}

Add reactions to a `GlobalModel`.

The reactions to be added are given as a
`COBREXA.CoreModel` either by components or as a single object. Note that you also
need to provide a `rxnPres`for the added reactions with an appropriate Boolean
vector/matrix.

NB behaviour might be unexpected if a reaction is already present.

"""
function COBREXA.add_reactions(gm::GlobalModel,
                              Sp::M,
                              b::V,
                              c::V,
                              xl::BV,
                              xu::BV,
                              rxns::K,
                              mets::K,
                              rxnPres::B) where {M<:MT,V<:VT,BV<:VT,K<:ST,B<:BMT}

    size(rxnPres) == (nModels(gm), length(rxns)) || throw(DimensionMismatch("`rxnPres` does not match with `nModels` and `nRxns`"))

    newLm = add_reactions(gm.coreModel, Sp, b, c, collect(xl), collect(xu), rxns, mets)
    iOfAdded = [findfirst(isequal(i), newLm.rxns) for i in rxns]
    order = [findfirst(isequal(i), rxns) for i in newLm.rxns[iOfAdded]]
    rest = setdiff(1:n_reactions(newLm), iOfAdded)
    newRxnPres = spzeros(Bool, nModels(gm), n_reactions(newLm))
    newRxnPres[:, iOfAdded] = rxnPres[:, order]
    newRxnPres[:, rest] = gm.rxnPres

    return GlobalModel(newLm, newRxnPres, gm.biomassRxns, gm.modelIDs)
end

"""
    add_reactions(
        gm::GlobalModel,
        Sp::M,
        b::V,
        c::V,
        xl::BV,
        xu::BV,
        rxns::K,
        mets::K,
        rxnPres::B
    ) where {M<:MT,V<:VT,BV<:VT,K<:ST,B<:BVT}

"""
function COBREXA.add_reactions(gm::GlobalModel,
                              Sp::M,
                              b::V,
                              c::V,
                              xl::BV,
                              xu::BV,
                              rxns::K,
                              mets::K,
                              rxnPres::B) where {M<:MT,V<:VT,BV<:VT,K<:ST,B<:BVT}

    return add_reactions(gm, Sp, b, c, xl, xu, rxns, mets, reshape(rxnPres, length(rxnPres), 1))
end

"""
    add_reactions(
        gm::GlobalModel,
        lm::CoreModel,
        rxnPres
        )

"""
function COBREXA.add_reactions(gm::GlobalModel,
                              lm::CoreModel,
                              rxnPres)

    return add_reactions(gm, lm.S, lm.b, lm.c, lm.xl, lm.xu, lm.rxns, lm.mets, rxnPres)
end

"""
    add_reactions(
        gm::GlobalModel,
        s::V1,
        b::V2,
        c::AbstractFloat,
        xl::AbstractFloat,
        xu::AbstractFloat,
        rxn::String,
        mets::K,
        rxnPres
    ) where {V1<:VT,V2<:VT,K<:ST}

"""
function COBREXA.add_reactions(gm::GlobalModel,
                              s::V1,
                              b::V2,
                              c::AbstractFloat,
                              xl::AbstractFloat,
                              xu::AbstractFloat,
                              rxn::String,
                              mets::K,
                              rxnPres) where {V1<:VT,V2<:VT,K<:ST}
    return add_reactions(gm, sparse(reshape(s, (length(s), 1))),
                        sparse(b), sparse([c]), [xl], [xu],
                        [rxn], mets, rxnPres)
end

"""
    removeModels(
        gModel::GlobalModel,
        models::Array{Int, 1}
        )

Remove model(s) from a `GlobalModel` by modelID or by index.

"""
function removeModels(gModel::GlobalModel, models::Array{Int, 1})
    any(models .> nModels(gModel)) && throw(ArgumentError("One or more indices in `models` exceeds total number of models"))
    remaining = setdiff(1:nModels(gModel), models)
    if isempty(remaining)
        return emptyGlobalModel()
    end
    newRxnPres = gModel.rxnPres[remaining, :]
    newBiomassRxns = gModel.biomassRxns[remaining]
    newModelIDs = gModel.modelIDs[remaining]

    orphaned = findall(all(.!newRxnPres, dims=1)[:])
    if isempty(orphaned)
        return GlobalModel(gModel.coreModel, newRxnPres, newBiomassRxns, newModelIDs)
    else
        return remove_reactions(GlobalModel(gModel.coreModel, newRxnPres, newBiomassRxns, newModelIDs),
                                    orphaned)
    end
end

"""
    removeModels(
        gModel::GlobalModel,
        models::Array{String, 1}
    )

"""
function removeModels(gModel::GlobalModel, models::Array{String, 1})
    modelInds = [findfirst(modelID.==gModel.modelIDs) for modelID in intersect(models, gModel.modelIDs)]
    if length(modelInds) < length(models)
        @warn "Some models were not found, ignoring"
    end
    if isempty(modelInds)
        return gModel
    else
        return removeModels(gModel, modelInds)
    end
end

"""
    removeModels(
        gModel::GlobalModel,
        models::Union{Integer, String}
    )

"""
function removeModels(gModel::GlobalModel, models::Union{Integer, String})
    return removeModels(gModel, [models])
end

"""
    mergeModels(
        gModel::GlobalModel,
        models::Array{Int64, 1},
        newModelName::String=""
    )

Merge `models` inside a `GlobalModel`.

The resulting model will be a union of the
reactions of `models`. A biomass reaction will be created as an average
of the biomass compositions of `models`. `models` will be deleted.
You can specify an ID for the new model by with the optional third argument.

# Example
```
gm = loadGlobalModel(joinpath("test", "data", "toyGlobalModel.mat"))
newGm = mergeModels(gm, [1;2])
```

"""
function mergeModels(gModel::GlobalModel, models::Array{Int64, 1}, newModelName::String="")
    any(models .> nModels(gModel)) && throw(ArgumentError("`models` exceeds number of models in GlobalModel"))
    if isempty(newModelName)
        newModelName = Base.join(gModel.modelIDs[models])
    end

    biomassRxnInds = [findfirst(id.==gModel.coreModel.rxns) for id in gModel.biomassRxns[models]]
    mergdBiomRxn = _mergeBiomassRxns(gModel.coreModel, biomassRxnInds, newModelName*"_mergedBiomass")
    mergdRxnPres = any(gModel.rxnPres[models, :], dims=1)[:]

    mergdLModel = CoreModel(gModel.coreModel.S[:, mergdRxnPres],
                             gModel.coreModel.b,
                             spzeros(size(gModel.coreModel.S[:, mergdRxnPres], 2)),
                             gModel.coreModel.xl[mergdRxnPres],
                             gModel.coreModel.xu[mergdRxnPres],
                             gModel.coreModel.rxns[mergdRxnPres],
                             gModel.coreModel.mets)
    mergdLModel = remove_reactions_with_metabolites(mergdLModel, Array{String, 1}(gModel.biomassRxns[models]))
    mergdLModel = add_reactions(mergdLModel, mergdBiomRxn...)
    newGModel = removeModels(gModel, models)

    return join(newGModel, mergdLModel, newModelName*"_mergedBiomass", newModelName)
end

"""
    mergeModels(
        gModel::GlobalModel,
        models::Array{String, 1},
        newModelName::String=""
    )

"""
function mergeModels(gModel::GlobalModel, models::Array{String, 1}, newModelName::String="")
    modelInds = [findfirst(modelID.==gModel.modelIDs) for modelID in intersect(models, gModel.modelIDs)]
    if length(modelInds) < length(models)
        @warn "Some models were not found, ignoring"
    end

    return mergeModels(gModel, modelInds, newModelName)
end

"""

Merge several biomass reactions.

Auxiliary function for `mergeModels`.

"""
function _mergeBiomassRxns(lModel, inds, name)
    mergdS = vec(sum(lModel.S[:, inds], dims=2)./length(inds))
    biomMetInd = findall(contains.(lModel.mets, "biomass") .& (mergdS.>0))
    if !isempty(biomMetInd)
        mergdS[biomMetInd] .= 0
        mergdS[biomMetInd[1]] = 1
    end

    return (mergdS[mergdS.!=0], lModel.b[mergdS.!=0], 1., sum(lModel.xl[inds])/length(inds),
                sum(lModel.xu[inds])/length(inds), name, lModel.mets[mergdS.!=0])
end

"""
    getModel(
        gModel::GlobalModel,
        model::Integer
    )

Get `model`as a `COBREXA.CoreModel` from a `GlobalModel` by index or modelID.

# Example
```
gm = loadGlobalModel(joinpath("test", "data", "toyGlobalModel.mat"))
model2 = getModel(gm, 2)
```

"""
function getModel(gModel::GlobalModel, model::Integer)
    model > nModels(gModel) && throw(ArgumentError("`model` exceeds number of models in GlobalModel"))
    rxnPres = gModel.rxnPres[model, :]
    metPres = vec(any(gModel.coreModel.S[:, rxnPres].!=0, dims=2))

    return CoreModel(gModel.coreModel.S[metPres, rxnPres],
                       gModel.coreModel.b[metPres],
                       gModel.coreModel.c[rxnPres],
                       gModel.coreModel.xl[rxnPres],
                       gModel.coreModel.xu[rxnPres],
                       gModel.coreModel.rxns[rxnPres],
                       gModel.coreModel.mets[metPres])
end

"""
    getModel(
        gModel::GlobalModel,
        model::String
    )

"""
function getModel(gModel::GlobalModel, model::String)
    modelInd = findfirst(model.==gModel.modelIDs)
    isnothing(modelInd) && throw(ArgumentError("Model named $model not found"))

    return getModel(gModel, modelInd)
end

"""
    find_exchange_reaction_metabolites(
        model::CoreModel;
        exclude_biomass = false,
        biomass_str::String = "biomass",
        exc_prefs = ["EX_"; "Exch_"; "Ex_"],
    )

Get indices of exchanged metabolites based on inferred exchange reactions
and stoichiometry.

In practice returns the metabolites consumed by the reactions given by
`COBREXA.find_exchange_reactions`
and if called with the same arguments, the two outputs correspond.

"""
function find_exchange_reaction_metabolites(
    model::CoreModel;
    exclude_biomass = false,
    biomass_str::String = "biomass",
    exc_prefs = ["EX_"; "Exch_"; "Ex_"],
)
    exc_rxn_inds = find_exchange_reactions(
        model,
        exclude_biomass = exclude_biomass,
        biomass_strings = [biomass_str],
        exchange_prefixes = exc_prefs,
    )
    exc_met_inds = [findfirst(x -> x == -1, model.S[:, j]) for j in exc_rxn_inds]
    return exc_met_inds
end

"""
    function find_exchange_reaction_metabolites(
        model::CoreModelCoupled;
        exclude_biomass = false,
        biomass_str::String = "biomass",
        exc_prefs = ["EX_"; "Exch_"; "Ex_"],
    )

"""
function find_exchange_reaction_metabolites(
    model::CoreModelCoupled;
    exclude_biomass = false,
    biomass_str::String = "biomass",
    exc_prefs = ["EX_"; "Exch_"; "Ex_"],
)
    return find_exchange_reaction_metabolites(
        model.lm,
        exclude_biomass=exclude_biomass,
        biomass_str=biomass_str,
        exc_prefs=exc_prefs)
end

remove_reactions_with_metabolites(m::CoreModel, rids::Vector{String}) =
    remove_reactions_with_metabolites(m, Int.(indexin(rids, reactions(m))))

function remove_reactions_with_metabolites(m::CoreModel, ridxs::Vector{Int})
    related_mets = Set(findnz(m.S[:,ridxs])[1])
    m = remove_reactions(m, ridxs)
    delete!.(Ref(related_mets), findnz(m.S)[1])
    return remove_metabolites(m, collect(related_mets))
end
