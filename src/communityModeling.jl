"""
    createCommunityReactions(
        model::GlobalModel
    )

Transform `model` into a community model.

Creates an additional "pseudomodel" to `model` called 'community' which represents the
shared extracellular space. For every metabolite 'm' exchanged by the individual
models in `model`, a community metabolite 'm(com)' and a community exchange
reaction 'EX_m[com]' are created and linked to the individual models. The
submodel exchange reaction are given an additional prefix 'I'.
A community biomass reaction is created which consumes "biomass molecules"
produced by individual biomass reactions.

See also [`expandCommunityModel`](@ref)

"""
function createCommunityReactions(model::GlobalModel)
    excRxnInds = find_exchange_reactions(model.coreModel, exclude_biomass=true)
    excMetInds = find_exchange_reaction_metabolites(model.coreModel, exclude_biomass=true)
    nExcs = length(excRxnInds)
    comExcMets = replace.(model.coreModel.mets[excMetInds], "[e]"=>"[com]")
    biomRxnInds = findall([(rxn in model.biomassRxns) for rxn in model.coreModel.rxns])
    biomS = model.coreModel.S[:, biomRxnInds]
    biomMetInds = findall([!all(biomS[i, :].==0) for i in 1:size(biomS, 1)])
    biomMetInds = setdiff(biomMetInds, findall(occursin.("biomass", model.coreModel.mets)))
    biomS = biomS[biomMetInds, :]
    biomS = [biomS; ones(1, size(biomS, 2))]
    biomExcInds = intersect(find_exchange_reactions(model.coreModel),
                            findall(occursin.("biomass", model.coreModel.rxns)))
    newModel = remove_reactions(model, [excRxnInds; biomRxnInds; biomExcInds], removeBiomass=true)
    newModel = add_reactions(newModel,
                            [-1*speye(nExcs); speye(nExcs)],
                            spzeros(2*nExcs),
                            spzeros(nExcs),
                            model.coreModel.xl[excRxnInds],
                            model.coreModel.xu[excRxnInds],
                            "I" .* model.coreModel.rxns[excRxnInds],
                            [model.coreModel.mets[excMetInds]; comExcMets],
                            model.rxnPres[:, excRxnInds])
    newModel = add_reactions(newModel,
                            biomS,
                            [model.coreModel.b[biomMetInds]; [0]],
                            spzeros(length(biomRxnInds)),
                            model.coreModel.xl[biomRxnInds],
                            model.coreModel.xu[biomRxnInds],
                            model.coreModel.rxns[biomRxnInds],
                            [model.coreModel.mets[biomMetInds]; "biomass[sub]"],
                            model.rxnPres[:, biomRxnInds])
    community = CoreModel(-1*speye(nExcs+1),
                            spzeros(nExcs+1),
                            [spzeros(nExcs); [1]],
                            [model.coreModel.xl[excRxnInds]; [0]],
                            [model.coreModel.xu[excRxnInds]; [1000]],
                            [replace.(model.coreModel.rxns[excRxnInds], r"\(e\)|\[e\]"=>"(com)");
                                "biomass(com)"],
                            [comExcMets; "biomass[sub]"])
    newModel = join(newModel, community, "biomass(com)", "community")

    return newModel
end

"""
    isCommunityModel(
        model::GlobalModel
    )

Check if a `GlobalModel` is a community model.

See also [`createCommunityReactions`](@ref)

"""
function isCommunityModel(model::GlobalModel)
    hasComComp = "community" in model.modelIDs
    excs = find_exchange_reactions(model.coreModel, exclude_biomass=true)
    hasComExcs = all(endswith.(model.coreModel.rxns[excs], "(com)"))
    return hasComComp && hasComExcs
end

"""
    expandCommunityModel(
        comModel::GlobalModel,
        abundances::V;
        scalingStrat=:abScale,
        coupBig=400,
        coupSmall=0
    ) where {V<:VT}

Expand `comModel` into a `COBREXA.CoreModel`.

`abuncances` is a vector giving the relative abundance for every model in `comModel`
(excluding the community pseudomodel). In the output `CoreModel` submodel-specific
reactions and metabolites are identified with modelID prefixes. The community
biomass reaction stoichiometry is set according to `abundances`, representing
the balanced growth of a community in ecological steady state.
Fluxes should be thought of as rate per total community biomass. To avoid
biologically infeasible fluxes submodel fluxes are bound according to relative
abundance. Two strategies are available:
`:abScale` Original bounds for all submodel reactions are scaled with relative abundance
`:couple` Submodel exchanges are "coupled" to submodel biomass reactions
            v_IEX - coupBig * v_biom <= coupSmall
            (use if original models lack realistic flux bounds)

See also: [`GlobalModel`](@ref), [`createCommunityReactions`](@ref)

# Example
```
gm = loadGlobalModel(joinpath("test", "data", "toyGlobalModel.mat"))
com = createCommunityReactions(gm)
expm = expandCommunityModel(com, [0.1; 0.1; 0.8])
```

"""
function expandCommunityModel(comModel::GlobalModel, abundances::V; scalingStrat=:abScale,
                            coupBig=400, coupSmall=0) where {V<:VT}
    isCommunityModel(comModel) || error("Model does not have community reactions. Run `createCommunityReactions()` first.")
    length(abundances) == length(comModel.modelIDs)-1 || error("`abundances` length doesn't match with the model.")
    ( all(abundances.>=0) & (abs(sum(abundances)-1) < 0.001) ) || error("Abundances should be non-negative and sum to 1")

    nonZeroAb = findall(x->x>0, abundances)
    modelIDs = [comModel.modelIDs[nonZeroAb]; comModel.modelIDs[end]]
    abundances = abundances[nonZeroAb]
    nSubModels = length(abundances)
    rxnPresVec = comModel.rxnPres[[nonZeroAb;end], :]'[:]

    S = kron([[speye(nSubModels) ones(nSubModels)]; ones(1, nSubModels+1)],
                comModel.coreModel.S)[:, rxnPresVec]
    metPresVec = repeat(.!endswith.(comModel.coreModel.mets, "[com]"), nSubModels)
    metPresVec = [metPresVec; endswith.(comModel.coreModel.mets, "[com]")]
    metPresVec = vec(metPresVec .& any(S.!=0, dims=2))
    S = S[metPresVec, :]
    b = repeat(comModel.coreModel.b, nSubModels+1)[metPresVec]
    c = repeat(comModel.coreModel.c, nSubModels+1)[rxnPresVec]
    xl = repeat(comModel.coreModel.xl, nSubModels+1)[rxnPresVec]
    xu = repeat(comModel.coreModel.xu, nSubModels+1)[rxnPresVec]
    if scalingStrat==:abScale
        xl = xl .* (repeat(permutedims([Array(abundances); 1]), length(comModel.coreModel.xl))[:][rxnPresVec])
        xu = xu .* (repeat(permutedims([Array(abundances); 1]), length(comModel.coreModel.xu))[:][rxnPresVec])
        # SparseVector is really slow here.
    end
    rxns = repeat(permutedims([modelIDs[1:end-1].*"_"; ""]),
                    n_reactions(comModel.coreModel))[:][rxnPresVec]
    rxns = rxns .* repeat(comModel.coreModel.rxns, nSubModels+1)[rxnPresVec]
    mets = repeat(permutedims([modelIDs[1:end-1].*"_"; ""]),
                    n_metabolites(comModel.coreModel))[:][metPresVec]
    mets = mets .* repeat(comModel.coreModel.mets, nSubModels+1)[metPresVec]

    biomMetInds = findall(S[:, findfirst(c.==1)].<0)
    S[biomMetInds, findfirst(isequal(1), c)] = -abundances

    if scalingStrat==:couple
        biomRxnInds = [findfirst(x->x>0, S[i, :]) for i in biomMetInds]
        (C, cl, cu) = _makeCouplingConstraints(comModel, nonZeroAb, rxns, biomRxnInds, xl, xu, coupBig, coupSmall)

        return CoreModelCoupled(CoreModel(S, b, c, xl, xu, rxns, mets), C, cl, cu)
    end

    return CoreModel(S, b, c, xl, xu, rxns, mets)
end

"""
Auxiliary function for `expandCommunityModel`.

Generates inequality constraints corresponding to coupling all exchange
reactions within a submodel to its biomass reaction
"""
function _makeCouplingConstraints(gModel, nonZeroAb, rxns, biomRxnInds, xl, xu, coupBig, coupSmall)
    IEXRxnInds = findall(contains.(rxns, "_IEX_"))
    nIEX = length(IEXRxnInds)
    toCoupleInds = CartesianIndex.(1:nIEX, IEXRxnInds)
    C = spzeros(nIEX, length(rxns))
    C[toCoupleInds] .= 1
    Crev = deepcopy(C)
    IEXPres = findall(((gModel.rxnPres[nonZeroAb, :].==1) .& repeat(startswith.(gModel.coreModel.rxns, "IEX")', length(nonZeroAb)))')
    for row in 1:nIEX
      C[row, biomRxnInds[IEXPres[row][2]]] = -coupBig
      Crev[row, biomRxnInds[IEXPres[row][2]]] = coupBig
    end
    cu = coupSmall .* sparse(ones(nIEX))
    cl = (minimum(xl) - coupBig*maximum(xu)) .* sparse(ones(nIEX))
    curev = (maximum(xu) + coupBig*maximum(xu)) .* sparse(ones(nIEX))
    clrev = -deepcopy(cu)
    C = [C; Crev]
    cl = [cl; clrev]
    cu = [cu; curev]

    return (C, cl, cu)
end

"""
    communityFBA(
        comModel::GlobalModel,
        abundanceVector::V,
        optimizer;
        scalingStrat=:abScale
    ) where {V<:VT}

Run flux balance analysis (FBA) on an implicitly defined community model.

Equivalent to expanding with `abundanceVector` and running `COBREXA.flux_balance_analysis`
but only returns flux values for community exchange reactions (which are independent
of `abundanceVector`).
`optimizer` is any `JuMP` compatible LP-solver.

See also [`createCommunityReactions`](@ref), [`COBREXA.flux_balance_analysis`](@ref)

# Example
```
using GLPK
gm = loadGlobalModel(joinpath("test", "data", "toyGlobalModel.mat"))
com = createCommunityReactions(gm)
(objectiveValue, exchangeFluxes) = communityFBA(com, [0.1;0.1;0.8], GLPK.Optimizer)
```

"""
function communityFBA(comModel::GlobalModel, abundanceVector::V, optimizer; scalingStrat=:abScale) where {V<:VT}
    isCommunityModel(comModel) || @error "`comModel` needs to be a community model"
    expModel = expandCommunityModel(comModel, abundanceVector, scalingStrat=scalingStrat)
    excs = find_exchange_reactions(expModel)
    optimization_model = COBREXA.flux_balance_analysis(expModel, optimizer)

    if termination_status(optimization_model) == MOI.OPTIMAL
        return (JuMP.objective_value(optimization_model), value.(JuMP.all_variables(optimization_model))[excs])
    else
        @warn "No optimum was found in FBA, model might be unviable"
        return (0, zeros(length(excs)))
    end
end


"""
    communityFVA(
        comModel::GlobalModel,
        abundanceVector::V,
        optimizer;
        gamma::AbstractFloat=1.0,
        scalingStrat=:abScale
    ) where {V<:VT}

Run flux variablity analysis (FVA) on an implicitly defined community model.

Equivalent to expanding with `abundanceVector` and running `COBREXA.flux_balance_analysis`
but only returns flux values for community exchange reactions (which are independent
of `abundanceVector`).
`optimizer` is any `JuMP` compatible LP-solver.
Use the parameter `gamma` to set a suboptimal objective constraint.

See also: [`createCommunityReactions`](@ref), [`COBREXA.flux_variability_analysis`](@ref)

# Example
```
using GLPK
gm = loadGlobalModel(joinpath("test", "data", "toyGlobalModel.mat"))
com = createCommunityReactions(gm)
fluxes = communityFVA(com, [0.1;0.1;0.8], GLPK.Optimizer)
```

"""
function communityFVA(comModel::GlobalModel, abundanceVector::V, optimizer; gamma::AbstractFloat=1.0, scalingStrat=:abScale) where {V<:VT}
    isCommunityModel(comModel) || @error "`comModel` needs to be a community model"
    expModel = expandCommunityModel(comModel, abundanceVector, scalingStrat=scalingStrat)
    excs = find_exchange_reactions(expModel)

    return flux_variability_analysis(expModel, excs, optimizer, bounds = z -> (gamma*z, z))
end

"""
    communityFVA(
        comModel::GlobalModel,
        abundanceVector::V,
        reactions::Vector{String},
        optimizer;
        gamma::AbstractFloat=1.0,
        scalingStrat=:abScale
    ) where {V<:VT}

Run FVA only on a subset of community exchanges.

`reactions` should contain reaction ids of community exchange reactions in
`comModel`. To see all community exchange reactions, run
`COBREXA.find_exchange_reactions(comModel.coreModel)`.

# Example
```
using GLPK
gm = loadGlobalModel(joinpath("test", "data", "toyGlobalModel.mat"))
com = createCommunityReactions(gm)
exchs = ["EX_m2(com)"; "EX_m4(com)"]
fluxes = communityFVA(com, [0.1;0.1;0.8], exchs, GLPK.Optimizer)
```

"""
function communityFVA(comModel::GlobalModel, abundanceVector::V, reactions::Vector{String}, optimizer; gamma::AbstractFloat=1.0, scalingStrat=:abScale) where {V<:VT}
    isCommunityModel(comModel) || @error "`comModel` needs to be a community model"

    expModel = expandCommunityModel(comModel, abundanceVector, scalingStrat=scalingStrat)
    excs = find_exchange_reactions(expModel)
    rxnIndices = [findfirst(isequal(name), expModel.rxns[excs]) for name in intersect(reactions, expModel.rxns[excs])]

    length(reactions) > length(rxnIndices) && error("Could not find one or more reactions in `reactions`")

    return flux_variability_analysis(expModel, excs[rxnIndices], optimizer, bounds = z -> (gamma*z, z))
end
