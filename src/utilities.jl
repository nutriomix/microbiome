function Base.isequal(model1::GlobalModel, model2::GlobalModel)
    return isequal(model1.coreModel, model2.coreModel) &&
                model1.rxnPres==model2.rxnPres &&
                model1.biomassRxns==model2.biomassRxns &&
                model1.modelIDs==model2.modelIDs
end


function Base.copy(model::GlobalModel)
    return GlobalModel(model.coreModel, model.rxnPres,
                       model.biomassRxns, model.modelIDs)
end

function Base.sort!(gm::GlobalModel)
    permRxns = sortperm(gm.coreModel.rxns)
    permMets = sortperm(gm.coreModel.mets)

    permute!(gm.coreModel.S, permMets, permRxns)
    permute!(gm.coreModel.b, permMets)
    permute!(gm.coreModel.c, permRxns)
    permute!(gm.coreModel.xl, permRxns)
    permute!(gm.coreModel.xu, permRxns)
    permute!(gm.coreModel.rxns, permRxns)
    permute!(gm.coreModel.mets, permMets)
    permute!(gm.rxnPres, 1:size(gm.rxnPres, 1), permRxns)

    return
end

function Base.isequal(ms1::MetaSet, ms2::MetaSet)
    return ms1.modelAbundances == ms2.modelAbundances &&
            ms1.sampleIDs == ms2.sampleIDs &&
            ms1.coverage == ms2.coverage &&
            ms1.removedSamples == ms2.removedSamples &&
            isequal(ms1.comModel, ms2.comModel) &&
            ms1.results.FBA == ms2.results.FBA &&
            ms1.results.FVA == ms2.results.FVA
end

"""
`n` by `n` sparse identity matrix.
"""
speye(n) = sparse(1.0I, n, n)
